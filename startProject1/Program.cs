﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace startProject1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //This way we can read options from the appsettings.json
            /*var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json");

            var config = builder.Build();
            var appConfig = new AppSettings();
            config.GetSection("ConnectionStrings").Bind(appConfig);

            var appConfig2 = config.GetSection("ConnectionStrings");*/

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
