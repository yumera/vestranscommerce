﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace startProject1.Models
{
    public class LoginController : Controller
    {

        [Display(Name = "Потребител: ")]
        [Required(ErrorMessage = "Моля попълнете потребител", AllowEmptyStrings = false)]
        public string userName { get; set; }
        [Display(Name = "Парола: ")]
        [Required]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string password { get; set; }

        public class connectionReturnState
        {
            public bool connectionBoolState { get; set; }
            public StringBuilder connectionStringState { get; set; }
            public string userEmail { get; set; }
            public string userRights { get; set; }
            public string userStation { get; set; }
        }

        public connectionReturnState checkUser(string vUserName, string vPassword, string connectionString)
        {
            connectionReturnState functionReturn = new connectionReturnState();
            StringBuilder errorMessages = new StringBuilder();
            connectionString = connectionString.Replace("@usrName", vUserName).Replace("@usrPass", vPassword);
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSql = @"select loginName, userEmail, userRight, stationName from [dbo].[vwUsers] where loginName = @u";
                        var cmd = new SqlCommand(vSql, conn);
                        cmd.Parameters.Add(new SqlParameter("@u", System.Data.SqlDbType.NVarChar)).Value = vUserName;
                        var reader = cmd.ExecuteReader();
                        functionReturn.connectionBoolState = (reader.HasRows);
                        functionReturn.connectionStringState = errorMessages.Append((functionReturn.connectionBoolState ? "Login successfull, usser is allowed" : "Грешка при опит на логин: Потребителя не е разрешен"));
                        if (reader.Read())
                        {
                            functionReturn.userEmail = reader["userEmail"].ToString();
                            functionReturn.userRights = reader["userRight"].ToString();
                            functionReturn.userStation = reader["stationName"].ToString();
                        }
                        return functionReturn;
                    }
                    else
                    {
                        functionReturn.connectionBoolState = false;
                        functionReturn.connectionStringState = errorMessages.Append("Login not successfull");
                        return functionReturn;
                    }
                }
                catch (SqlException ex)
                {
                    functionReturn.connectionBoolState = false;
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        if (ex.Errors[0].ToString().IndexOf("Error Locating Server/Instance") > 0) {
                            errorMessages.Append("Грешка при опит на логин: Не може да се свържи с сървъра.");
                        } else { 
                            errorMessages.Append( "Грешка при опит на логин: Невалиден потребител или парола.");
                        }
                    }
                    functionReturn.connectionStringState = errorMessages;
                    return functionReturn;
                }
                catch (Exception ex)
                {
                    functionReturn.connectionBoolState = false;
                    functionReturn.connectionStringState = errorMessages.Append("Грешка при опит на логин : Системна грешка.");
                    return functionReturn;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}