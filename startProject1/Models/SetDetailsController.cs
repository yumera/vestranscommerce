﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace startProject1.Models
{
    public class SetDetailsController : Controller
    {
        [Display(Name = "Номер на поръчка")]
        public string orderId { get; set; }

        [Display(Name = "Въведете детайли")]
        public string orderDetailDescription { get; set; }

        [Display(Name = "Станиция")]
        public SelectList stationId { get; set; }

        [Display(Name = "Статус")]
        [Required(ErrorMessage = "Статус е задължителен", AllowEmptyStrings = false)]
        public SelectList statusId { get; set; }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        public class returnStateWithList
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public int totalCount { get; set; }
            public List<orderDetails> returnObject;
        }

        [Authorize]
        public class selectClass
        {
            public string key { get; set; }
            public string value { get; set; }
        }

        [Authorize]
        public class orderDetails
        {
            public int autoId { get; set; }
            public Int64 rowNumber { get; set; }
            public string orderId { get; set; }
            public string stationName { get; set; }
            public string userNames { get; set; }
            public string orderDetailDescription { get; set; }
            public string statusType { get; set; }
            public DateTime orderDetailCreateDT { get; set; }
            public Boolean statusOrderFinished { get; set; }
            public string canvasAsObject { get; set; }
            public string canvasAsObjectCollected { get; set; }
        }

        [Authorize]
        public class newOrderDetails
        {
            public string orderId { get; set; }
            public int stationId { get; set; }
            public int userId { get; set; }
            public string orderDetailDescription { get; set; }
            public int statusId { get; set; }
            public string canvasAsObject { get; set; }
            public string canvasAsObjectCollected { get; set; }
        }

        [Authorize]
        public List<selectClass> fillSelectForOrderDetails(string connectionSting, string sqlToExecute)
        {
            List<selectClass> vReturnSelect = new List<selectClass>();

            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = sqlToExecute;
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            vReturnSelect.Add(new selectClass
                            {
                                key = reader["autoId"].ToString() + ( reader.GetInt32( reader.GetOrdinal("dataSelect") ) > 0 ? "_1" : "" ),
                                value = reader["name"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }


        [Authorize]
        public returnStateWithList getOrderDetails(string userName, string orderId, string connectionString)
        {
            returnStateWithList returnList = new returnStateWithList();
            returnList.returnObject = new List<orderDetails>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[rowNumber],[orderId],[stationName],[userNames],[orderDetailDescription],[statusId],[orderDetailCreateDT],[statusDescription],[statusType],[statusOrderFinished], [canvasAsObject], [canvasAsObjectCollected] FROM [dbo].[vwOrderDetails] where orderId = '" + orderId + "' order by [autoId],[rowNumber]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addOrder = new orderDetails();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.rowNumber = reader.GetInt64(reader.GetOrdinal("rowNumber"));
                            addOrder.orderId = reader["orderId"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            addOrder.userNames = reader["userNames"].ToString();
                            addOrder.orderDetailDescription = reader["orderDetailDescription"].ToString();
                            addOrder.statusType = reader["statusType"].ToString();
                            addOrder.orderDetailCreateDT = reader.GetDateTime(reader.GetOrdinal("orderDetailCreateDT"));
                            addOrder.statusOrderFinished = reader.GetBoolean(reader.GetOrdinal("statusOrderFinished"));
                            addOrder.canvasAsObject = reader["canvasAsObject"].ToString();
                            addOrder.canvasAsObjectCollected = reader["canvasAsObjectCollected"].ToString();
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";

                        returnList.totalCount = (int)com.ExecuteScalar(); ;
                        returnList.resultState = true;
                        returnList.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

        [AllowAnonymous]
        public returnStateWithList getOrderDetailsAnon(string userName, string orderId, string connectionString)
        {
            returnStateWithList returnList = new returnStateWithList();
            returnList.returnObject = new List<orderDetails>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[rowNumber],[orderId],[stationName],[userNames],[orderDetailDescription],[statusId],[orderDetailCreateDT],[statusDescription],[statusType] FROM [dbo].[vwOrderDetails] where orderId = '" + orderId + "' order by [autoId],[rowNumber]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addOrder = new orderDetails();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.rowNumber = reader.GetInt64(reader.GetOrdinal("rowNumber"));
                            addOrder.orderId = reader["orderId"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            addOrder.userNames = reader["userNames"].ToString();
                            addOrder.orderDetailDescription = reader["orderDetailDescription"].ToString();
                            addOrder.statusType = reader["statusType"].ToString();
                            addOrder.orderDetailCreateDT = reader.GetDateTime(reader.GetOrdinal("orderDetailCreateDT"));
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";

                        returnList.totalCount = (int)com.ExecuteScalar(); ;
                        returnList.resultState = true;
                        returnList.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

        [Authorize]
        public returnState createOrderDetails(newOrderDetails newOrderDetails, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[createOrderDetails]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@orderId", SqlDbType.NVarChar).Value = newOrderDetails.orderId;
                        cmd.Parameters.Add("@stationId", SqlDbType.Int).Value = newOrderDetails.stationId;
                        cmd.Parameters.Add("@userId", SqlDbType.Int).Value = newOrderDetails.userId;
                        cmd.Parameters.Add("@orderDetailDescription", SqlDbType.NVarChar).Value = newOrderDetails.orderDetailDescription;
                        cmd.Parameters.Add("@statusId", SqlDbType.Int).Value = newOrderDetails.statusId;
                        cmd.Parameters.Add("@canvasAsObject", SqlDbType.NVarChar).Value = newOrderDetails.canvasAsObject;
                        cmd.Parameters.Add("@canvasAsObjectCollected", SqlDbType.NVarChar).Value = newOrderDetails.canvasAsObjectCollected;

                        SqlParameter returnParameter = new SqlParameter("@return_autoId", SqlDbType.Int) { Direction = ParameterDirection.Output };
                        cmd.Parameters.Add(returnParameter);

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnState;
        }

    }
}