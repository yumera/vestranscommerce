﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace startProject1.Models
{
    public class ReportsModel : Controller
    {
        [Display(Name = "Справка име")]
        [Required(ErrorMessage = "Моля изберете справка", AllowEmptyStrings = false)]
        public SelectList reportNameList { get; set; }

        [Authorize]
        public class reportSelectClass
        {
            public string key { get; set; }
            public string reportName { get; set; }
        }

        [Authorize]
        public class reportParametersReturnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public string resultParameters { get; set; }
        }

        [Authorize]
        public List<reportSelectClass> fillSelectForReports(string connectionSting, string sqlToExecute)
        {
            List<reportSelectClass> vReturnSelect = new List<reportSelectClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = sqlToExecute;
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new reportSelectClass
                            {
                                key = reader["autoId"].ToString(),
                                reportName = reader["name"].ToString()
                            });
                        }
                    }
                }

                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        [Authorize]
        public reportParametersReturnState callReportByName(string userName, int reportId, string connectionString, dynamic parametersObj)
        {
            var fnResult = new reportParametersReturnState();
            var jsonOutputParam = "@jsonOutData";
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    var reportName = getReportDataById(userName, reportId, connectionString, "reportSqlName").resultParameters;
                    var reportParametersString = getReportDataById(userName, reportId, connectionString, "reportParameters").resultParameters;
                    //var reportName = "dbo.testReport1";
                    using (SqlCommand cmd = new SqlCommand(reportName, conn)){
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        foreach (var reportObj in parametersObj){
                            SqlDbType dbType = (SqlDbType)Enum.Parse(typeof(SqlDbType), reportObj.parameterType.Value);
                            cmd.Parameters.Add("@" + reportObj.parameterId.Value, dbType, -1).Value = (
                                dbType == SqlDbType.Date ? DateTime.ParseExact(reportObj.parameterValue.Value.ToString(), "dd.MM.yyyy", null)
                                : reportObj.parameterValue.Value );
                        }

                        cmd.Parameters.Add(jsonOutputParam, SqlDbType.NVarChar, -1).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        // Get the values
                        fnResult.resultParameters = cmd.Parameters[jsonOutputParam].Value.ToString();
                        fnResult.resultState = true;
                        fnResult.resultMessage = "Success";
                    }
                }
                catch (SqlException ex){
                    fnResult.resultState = false;
                    fnResult.resultMessage = "error on execute callReportByName : " + ex.Message;
                }
                catch (Exception ex){
                    fnResult.resultState = false;
                    fnResult.resultMessage = "error on execute callReportByName : " + ex.Message;
                }
                finally{
                    fnResult.resultState = true;
                    fnResult.resultMessage = "Success";
                    conn.Close();
                }

            }
            return fnResult;
        }

        [Authorize]
        public reportParametersReturnState getReportDataById(string userName, int reportId, string connectionString, string fieldName){
            var fnReturn = new reportParametersReturnState();

            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "select isNull( ["+ fieldName + "], '' ) as reportParameters from dbo.reports where autoId = " + reportId + " and reportEnabled = 1 and reportDeleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            fnReturn.resultParameters = reader["reportParameters"].ToString();
                        }
                        fnReturn.resultState = true;
                        fnReturn.resultMessage = "Success";
                    }
                }

                catch (SqlException ex){
                    fnReturn.resultState = false;
                    fnReturn.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    fnReturn.resultState = false;
                    fnReturn.resultMessage = ex.Message;
                }
                finally{
                    fnReturn.resultState = true;
                    fnReturn.resultMessage = "Success";
                    conn.Close();
                }

            }

            return fnReturn;
        }
    }


}
