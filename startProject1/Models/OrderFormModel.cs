﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace startProject1.Models
{
    public class OrderFormModel : Controller
    {
        /*Start contact sender information*/
        [Display(Name = "Име на фирма")]
        public string contactCompanyName { get; set; }

        [Display(Name = "Име")]
        [Required(ErrorMessage = "Моля попълнете име", AllowEmptyStrings = false)]
        public string contactFirstName { get; set; }

        [Display(Name = "Презиме")]
        [Required(ErrorMessage = "Моля попълнете Презиме", AllowEmptyStrings = false)]
        public string contactMidleName { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Моля попълнете Фамилия", AllowEmptyStrings = false)]
        public string contactLastName { get; set; }

        [Display(Name = "Държава")]
        [Required(ErrorMessage = "Моля попълнете Държава", AllowEmptyStrings = false)]
        public string contactCountry { get; set; }

        [Display(Name = "Град")]
        [Required(ErrorMessage = "Моля попълнете Град", AllowEmptyStrings = false)]
        public string contactCity { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Моля попълнете Адрес", AllowEmptyStrings = false)]
        public string contactAddress { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Моля попълнете Телефон", AllowEmptyStrings = false)]
        public string contactPhone { get; set; }
        /*End contact sender information*/

        /*Start other type */
        [Display(Name = "Тип поръчка")]
        [Required(ErrorMessage = "Моля попълнете тип поръчка", AllowEmptyStrings = false)]
        public SelectList orderTypesList { get; set; }

        [Display(Name = "Тип плащане")]
        [Required(ErrorMessage = "Моля попълнете тип плащане", AllowEmptyStrings = false)]
        public SelectList cashOnDeliveryTypeList { get; set; }

        /*End other type */



        [Display(Name = "Поръчка тип")]
        [Required(ErrorMessage = "Моля попълнете тип на поръчка", AllowEmptyStrings = false)]
        public int orderTypeId { get; set; }

        [Display(Name = "Поръчката е документ")]
        public bool orderIsDocument { get; set; }

        [Display(Name = "Брой")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        public int orderCount { get; set; }

        [Display(Name = "Описание на поръчката")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        public string orderDescription { get; set; }

        [Display(Name = "Описание на съдържанието")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        public string orderContent { get; set; }

        [Display(Name = "Тегло (кг.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        [Range(0, float.MaxValue, ErrorMessage = "Въведената стойност трябва да е по голяма от 0")]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public float orderWeight { get; set; }

        [Display(Name = "Широчина (см.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public float orderWidth { get; set; }

        [Display(Name = "Дължина (см.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public float orderHeight { get; set; }

        [Display(Name = "Дълбочина (см.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public float orderDepth { get; set; }

        [Display(Name = "Наложен платеж (лв.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = false)]
        public bool chashOnDelivery { get; set; }

        [Display(Name = "Сума наложен платеж (лв.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = true)]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public decimal chashOnDeliverySum { get; set; }

        [Display(Name = "Сума на застраховка (лв.)")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = true)]
        [RegularExpression("^\\-{0,1}\\d+(.\\d+){0,1}$", ErrorMessage = "Въведената стойност трябва да е число")]
        public decimal orderInsuranceSum { get; set; }


        [Display(Name = "Тип на плащане")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = true)]
        public int cashOnDeliveryTypeId { get; set; }

        [Display(Name = "Дата на доставка")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = true)]
        public DateTime orderDeliveryDate { get; set; }

        [DataType(DataType.Time)]
        [Display(Name = "Час на доставка")]
        [Required(ErrorMessage = "Задължително", AllowEmptyStrings = true)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public TimeSpan orderDeliveryTime { get; set; }

        [Display(Name = "Поръчки за град")]
        public SelectList ordersToCity { get; set; }

        [Display(Name = "Ценова група")]
        public SelectList priceGroupList { get; set; }

        [Display(Name = "Станиция")]
        [Required(ErrorMessage = "Станиция е задължителена", AllowEmptyStrings = false)]
        public SelectList stationIdList { get; set; }

        [Display(Name = "Куриери")]
        [Required(ErrorMessage = "Куриер е задължителен", AllowEmptyStrings = false)]
        public SelectList courierIdList { get; set; }

        [Display(Name = "Поръчки от град")]
        public SelectList ordersFromCity { get; set; }

        public class stationIdListClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        public class courierIdListClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public string resultOrderId { get; set; }
        }

        public class returnStateFullOrder {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public List<FullOrders> returnObject;
        }

        public class returnStateWithList
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public int totalCount { get; set; }
            public int filteredCount { get; set; }
            public List<Contacts> returnObject;
            public List<Receiver> returnObjectReceiver;
        }

        public class dataTableReturnClass
        {
            public int totalRecords { get; set; }
            public int filteredRecords { get; set; }
        }

        public class returnStateWithOrdersMainView
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public int totalCount { get; set; }
            public int filteredCount { get; set; }
            public List<OrdersMainView> returnObject;
        }

        public class buildWhereClause
        {
            public string paramName {get; set;}
            public string paramValue { get; set; }
        }

        public class contactPriceDetails
        {
            public string priceGroupName { get; set; }
            public int discountPersantage { get; set; }
        }


        [Authorize]
        public class Contacts
        {
            public int autoId { get; set; }
            public string contactCompanyName { get; set; }
            public string contactFirstName { get; set; }
            public string contactMidleName { get; set; }
            public string contactLastName { get; set; }
            public string contactCountry { get; set; }
            public string contactCity { get; set; }
            public string contactAddress { get; set; }
            public string contactPhone { get; set; }
            public string contactOthers { get; set; }
            public string priceGroupName { get; set; }
            public contactPriceDetails priceDetails { get; set; }
            public string conctactNearestStationName { get; set; }
        }

        [Authorize]
        public class Receiver
        {
            public int autoId { get; set; }
            public string receiverCompanyName { get; set; }
            public string receiverFirstName { get; set; }
            public string receiverMidleName { get; set; }
            public string receiverLastName { get; set; }
            public string receiverCountry { get; set; }
            public string receiverCity { get; set; }
            public string receiverAdress { get; set; }
            public string receiverPhone { get; set; }
            public string receiverOthers { get; set; }
            public string receiverPriceGroupName { get; set; }
            public contactPriceDetails receiverPriceDetails { get; set; }
            public string receiverConctactNearestStationName { get; set; }
        }

        [Authorize]
        public class FullOrders
        {
            public int autoId { get; set; }
            public string orderId { get; set; }
            public string CreatedBy { get; set; }
            public string stationName { get; set; }
            public string stationCity { get; set; }
            public string stationAddress { get; set; }
            public int orderSenderId { get; set; }
            public string contactCity { get; set; }
            public string contactAddress { get; set; }
            public string contactFirstName { get; set; }
            public string contactMidleName { get; set; }
            public string contactLastName { get; set; }
            public string contactPhone { get; set; }
            public string contactCompanyName { get; set; }
            public contactPriceDetails priceDetails { get; set; }
            public int orderReceiverId { get; set; }
            public string receiverCity { get; set; }
            public string receiverAdress { get; set; }
            public string receiverFirstName { get; set; }
            public string receiverMidleName { get; set; }
            public string receiverLastName { get; set; }
            public string receiverPhone { get; set; }
            public string receiverCompanyName { get; set; }
            public contactPriceDetails receiverPriceDetails { get; set; }
            public int orderTypeId { get; set; }
            public string orderTypeName { get; set; }
            public bool orderIsDocument { get; set; }
            public string orderDescription { get; set; }
            public string orderContent { get; set; }
            public int orderCount { get; set; }
            public Double orderWeight { get; set; }
            public Double orderWidth { get; set; }
            public Double orderHeight { get; set; }
            public Double orderDepth { get; set; }
            public bool chashOnDelivery { get; set; }
            public decimal chashOnDeliverySum { get; set; }
            public decimal orderInsuranceSum { get; set; }
            public int cashOnDeliveryTypeId { get; set; }
            public string chashOnDeliveryName { get; set; }
            public DateTime orderDeliveryDateTime { get; set; }
            public DateTime orderDeliveryDate { get; set; }
            public TimeSpan orderDeliveryTime { get; set; }
            public DateTime orderCreatedDateTime { get; set; }
            public int orderCityPriceId { get; set; }
            public int orderInterCityPriceId { get; set; }
            public string orderExtraPricesId { get; set; }
            public bool orderInterCityPriceIdExpress { get; set; }
            public decimal orderPriceVat { get; set; }
            public decimal orderPriceNoVat { get; set; }
            public string cityDescription { get; set; }
            public string interCityDescription { get; set; }
            public string deliveryType { get; set; }
        }

        [Authorize]
        public class OrdersMainView
        {
            public int autoId { get; set; }
            public string orderId { get; set; }
            public string CreatedBy { get; set; }
            public string stationName { get; set; }
            public string contactCity { get; set; }
            public string contactAddress { get; set; }
            public string contactPhone { get; set; }
            public string contactCompanyName { get; set; }
            public string senderNearestStationName { get; set; }
            public string receiverCity { get; set; }
            public string receiverPhone { get; set; }
            public string receiverCompanyName { get; set; }
            public string receiverAddress { get; set; }
            public string orderTypeName { get; set; }
            public bool orderIsDocument { get; set; }
            public int orderCount { get; set; }
            public bool chashOnDelivery { get; set; }
            public decimal chashOnDeliverySum { get; set; }
            public decimal orderInsuranceSum { get; set; }
            public string chashOnDeliveryName { get; set; }
            public DateTime orderDeliveryDateTime { get; set; }
            public DateTime orderDeliveryDate { get; set; }
            public TimeSpan orderDeliveryTime { get; set; }
            public DateTime orderCreatedDateTime { get; set; }
            public decimal orderPriceVat { get; set; }
            public decimal orderPriceNoVat { get; set; }
            public string orderDescriptionStationName { get; set; }
            public string statusDescription { get; set; }
            public bool statusOrderFinished { get; set; }
            public string nearestStationName { get; set; }
            public Int32 priorityOrder { get; set; }
            public string courierLoginName { get; set; }
        }

        [Authorize]
        public class newOrderClass
        {
            public string orderId { get; set; }
            public int orderCreatedByUserID { get; set; }
            public int stationID { get; set; }
            public int orderSenderId { get; set; }
            public int orderReceiverId { get; set; }
            public int orderTypeId { get; set; }
            public bool orderIsDocument { get; set; }
            public int orderCount { get; set; }
            public string orderDescription { get; set; }
            public string orderContent { get; set; }
            public float orderWeight { get; set; }
            public float orderWidth { get; set; }
            public float orderHeight { get; set; }
            public float orderDepth { get; set; }
            public bool chashOnDelivery { get; set; }
            public decimal chashOnDeliverySum { get; set; }
            public decimal orderInsuranceSum { get; set; }
            public int cashOnDeliveryTypeId { get; set; }
            public DateTime orderDeliveryDate { get; set; }
            public TimeSpan orderDeliveryTime { get; set; }
            public DateTime orderCreatedDateTime { get; set; }
            public int orderCityPriceId { get; set; }
            public int orderInterCityPriceId { get; set; }
            public bool orderInterCityPriceIdExpress { get; set; }
            public string orderExtraPricesId { get; set; }
            public decimal orderPriceVat { get; set; }
            public decimal orderPriceNoVat { get; set; }
            public string orderImportFileName { get; set; }
        }

        [Authorize]
        public class orderSelectClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        [Authorize]
        public List<stationIdListClass> htmlStationIdListClass(string connectionSting){
            List<stationIdListClass> vReturnSelect = new List<stationIdListClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        vReturnSelect.Add(new stationIdListClass
                        {
                            key = "-1",
                            selectLabel = ""
                        });

                        string vSQLString = "select autoId = cast(autoId as nvarchar(50)) + '_{ \"latitude\" : \"' + Cast( IsNull( stationGeoLocation.Lat, 0.0 ) as nvarchar(50) ) + '\",\"longitude\":\"' + cast( IsNull( stationGeoLocation.Long, 0.0 )  as nvarchar(50) ) + '\"}' , stationName from [dbo].[stations] with (nolock) where stationEnabled = 1 and stationDeleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new stationIdListClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["stationName"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        [Authorize]
        public List<courierIdListClass> htmlCourierIdListClass(string connectionSting){
            List<courierIdListClass> vReturnSelect = new List<courierIdListClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        vReturnSelect.Add(new courierIdListClass
                        {
                            key = "-1",
                            selectLabel = ""
                        });

                        string vSQLString = "select autoId, loginName from vwusers where userRightCode = 'Courier'";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new courierIdListClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["loginName"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }


        public returnState saveOrder(newOrderClass newOrder, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[createOrder]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@orderCreatedByUserID", SqlDbType.Int).Value = newOrder.orderCreatedByUserID;
                        cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = newOrder.stationID;
                        cmd.Parameters.Add("@orderSenderId", SqlDbType.Int).Value = newOrder.orderSenderId;
                        cmd.Parameters.Add("@orderReceiverId", SqlDbType.Int).Value = newOrder.orderReceiverId;
                        cmd.Parameters.Add("@orderTypeId", SqlDbType.Int).Value = newOrder.orderTypeId;
                        cmd.Parameters.Add("@orderIsDocument", SqlDbType.Bit).Value = newOrder.orderIsDocument;
                        cmd.Parameters.Add("@orderCount", SqlDbType.Int).Value = newOrder.orderCount;
                        cmd.Parameters.Add("@orderDescription", SqlDbType.NVarChar).Value = newOrder.orderDescription;
                        cmd.Parameters.Add("@orderContent", SqlDbType.NVarChar).Value = newOrder.orderContent;
                        cmd.Parameters.Add("@orderWeight", SqlDbType.Float).Value = newOrder.orderWeight;
                        cmd.Parameters.Add("@orderWidth", SqlDbType.Float).Value = newOrder.orderWidth;
                        cmd.Parameters.Add("@orderHeight", SqlDbType.Float).Value = newOrder.orderHeight;
                        cmd.Parameters.Add("@orderDepth", SqlDbType.Float).Value = newOrder.orderDepth;
                        cmd.Parameters.Add("@chashOnDelivery", SqlDbType.Bit).Value = newOrder.chashOnDelivery;
                        cmd.Parameters.Add("@chashOnDeliverySum", SqlDbType.Decimal).Value = newOrder.chashOnDeliverySum;
                        cmd.Parameters.Add("@orderInsuranceSum", SqlDbType.Decimal).Value = newOrder.orderInsuranceSum;
                        cmd.Parameters.Add("@cashOnDeliveryTypeId", SqlDbType.Int).Value = newOrder.cashOnDeliveryTypeId;
                        cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.Date).Value = newOrder.orderDeliveryDate;
                        cmd.Parameters.Add("@orderDeliveryTime", SqlDbType.Time).Value = newOrder.orderDeliveryTime;
                        cmd.Parameters.Add("@orderEnabled", SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add("@orderCityPriceId", SqlDbType.Int).Value = newOrder.orderCityPriceId;
                        cmd.Parameters.Add("@orderInterCityPriceId", SqlDbType.Int).Value = newOrder.orderInterCityPriceId;
                        cmd.Parameters.Add("@orderExtraPricesId", SqlDbType.NVarChar).Value = newOrder.orderExtraPricesId;
                        cmd.Parameters.Add("@orderInterCityPriceIdExpress", SqlDbType.Bit).Value = newOrder.orderInterCityPriceIdExpress;
                        cmd.Parameters.Add("@orderPriceVat", SqlDbType.Decimal).Value = newOrder.orderPriceVat;
                        cmd.Parameters.Add("@orderPriceNoVat", SqlDbType.Decimal).Value = newOrder.orderPriceNoVat;
                        cmd.Parameters.Add("@orderImportFileName", SqlDbType.NVarChar).Value = newOrder.orderImportFileName;

                        /*var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                        var returnParameterOrderId = cmd.Parameters.Add("@return_orderId", SqlDbType.NVarChar);
                        returnParameter.Direction = ParameterDirection.ReturnValue;
                        returnParameterOrderId.Direction = ParameterDirection.ReturnValue;*/

                        SqlParameter returnParameter = new SqlParameter("@return_autoId", SqlDbType.Int) { Direction = ParameterDirection.Output };
                        cmd.Parameters.Add(returnParameter);

                        SqlParameter returnParameterOrderId = new SqlParameter("@return_orderId", SqlDbType.NVarChar) { Direction = ParameterDirection.Output };
                        returnParameterOrderId.Size = 150;
                        cmd.Parameters.Add(returnParameterOrderId);

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);
                        vReturnState.resultMessage = "Saved succesfully";
                        vReturnState.resultOrderId = (string)returnParameterOrderId.Value;
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnState;
        }

        [Authorize]
        public returnState editOrder(newOrderClass editOrder, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[editOrder]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@orderId", SqlDbType.NVarChar).Value = editOrder.orderId;
                        cmd.Parameters.Add("@stationID", SqlDbType.Int).Value = editOrder.stationID;
                        cmd.Parameters.Add("@orderSenderId", SqlDbType.Int).Value = editOrder.orderSenderId;
                        cmd.Parameters.Add("@orderReceiverId", SqlDbType.Int).Value = editOrder.orderReceiverId;
                        cmd.Parameters.Add("@orderTypeId", SqlDbType.Int).Value = editOrder.orderTypeId;
                        cmd.Parameters.Add("@orderIsDocument", SqlDbType.Bit).Value = editOrder.orderIsDocument;
                        cmd.Parameters.Add("@orderCount", SqlDbType.Int).Value = editOrder.orderCount;
                        cmd.Parameters.Add("@orderDescription", SqlDbType.NVarChar).Value = editOrder.orderDescription;
                        cmd.Parameters.Add("@orderContent", SqlDbType.NVarChar).Value = editOrder.orderContent;
                        cmd.Parameters.Add("@orderWeight", SqlDbType.Float).Value = editOrder.orderWeight;
                        cmd.Parameters.Add("@orderWidth", SqlDbType.Float).Value = editOrder.orderWidth;
                        cmd.Parameters.Add("@orderHeight", SqlDbType.Float).Value = editOrder.orderHeight;
                        cmd.Parameters.Add("@orderDepth", SqlDbType.Float).Value = editOrder.orderDepth;
                        cmd.Parameters.Add("@chashOnDelivery", SqlDbType.Bit).Value = editOrder.chashOnDelivery;
                        cmd.Parameters.Add("@chashOnDeliverySum", SqlDbType.Decimal).Value = editOrder.chashOnDeliverySum;
                        cmd.Parameters.Add("@orderInsuranceSum", SqlDbType.Decimal).Value = editOrder.orderInsuranceSum;
                        cmd.Parameters.Add("@cashOnDeliveryTypeId", SqlDbType.Int).Value = editOrder.cashOnDeliveryTypeId;
                        cmd.Parameters.Add("@orderDeliveryDate", SqlDbType.Date).Value = editOrder.orderDeliveryDate;
                        cmd.Parameters.Add("@orderDeliveryTime", SqlDbType.Time).Value = editOrder.orderDeliveryTime;
                        cmd.Parameters.Add("@orderEnabled", SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add("@orderCityPriceId", SqlDbType.Int).Value = editOrder.orderCityPriceId;
                        cmd.Parameters.Add("@orderInterCityPriceId", SqlDbType.Int).Value = editOrder.orderInterCityPriceId;
                        cmd.Parameters.Add("@orderExtraPricesId", SqlDbType.NVarChar).Value = editOrder.orderExtraPricesId;
                        cmd.Parameters.Add("@orderInterCityPriceIdExpress", SqlDbType.Bit).Value = editOrder.orderInterCityPriceIdExpress;
                        cmd.Parameters.Add("@orderPriceVat", SqlDbType.Decimal).Value = editOrder.orderPriceVat;
                        cmd.Parameters.Add("@orderPriceNoVat", SqlDbType.Decimal).Value = editOrder.orderPriceNoVat;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                        vReturnState.resultOrderId = (string)editOrder.orderId;
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnState;
        }

        [Authorize]
        public returnState deleteOrder(newOrderClass editOrder, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[editOrder]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@orderId", SqlDbType.Int).Value = editOrder.orderId;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnState;
        }


        [Authorize]
        public List<orderSelectClass> fillSelectForOrders(string connectionSting, string sqlToExecute)
        {
            List<orderSelectClass> vReturnSelect = new List<orderSelectClass>();

            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = sqlToExecute;
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new orderSelectClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["name"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        [Authorize]
        public returnStateFullOrder getOrderFullViewList(string userName, string whereClause, string orderId, string connectionString)
        {
            returnStateFullOrder vReturnList = new returnStateFullOrder();
            vReturnList.returnObject = new List<FullOrders>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "SELECT [autoId],[orderId],[CreatedBy],[stationName],[orderSenderId],[stationCity],[stationAddress],[contactCity],[contactAddress],[contactPhone],[contactFirstName], [contactMidleName], [contactLastName],[contactCompanyName],[orderReceiverId],[receiverCity],[receiverPhone],[receiverAdress], [receiverFirstName], [receiverMidleName], [receiverLastName],[receiverCompanyName],[orderTypeId],[orderTypeName],[orderIsDocument],[orderCount],[cashOnDeliveryTypeId], [chashOnDeliveryName], [chashOnDelivery],[chashOnDeliverySum], [orderPriceVat], [orderPriceNoVat], [orderInsuranceSum],[orderDeliveryDateTime],[orderDeliveryDate],[orderDeliveryTime],[orderCreatedDateTime], [orderDescription], [orderContent], [orderWeight], [orderHeight], [orderWidth], [orderDepth], [orderSenderId], [orderCityPriceId], [orderExtraPricesId], [orderInterCityPriceId], [orderInterCityPriceIdExpress], [cityDescription], [interCityDescription], [deliveryType], [priceGroupName], [priceGroupDiscountPercent], [receiverPriceGroupName], [receiverPriceGroupDiscountPercent] FROM [dbo].[vwFullOrder] " + whereClause;
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@orderId", orderId);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addOrder = new FullOrders();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.orderId = reader["orderId"].ToString();
                            addOrder.CreatedBy = reader["CreatedBy"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            addOrder.stationCity = reader["stationCity"].ToString();
                            addOrder.stationAddress = reader["stationAddress"].ToString();
                            addOrder.orderSenderId = reader.GetInt32(reader.GetOrdinal("orderSenderId"));
                            addOrder.contactCity = reader["contactCity"].ToString();
                            addOrder.contactAddress = reader["contactAddress"].ToString();
                            addOrder.contactFirstName = reader["contactFirstName"].ToString();
                            addOrder.contactMidleName = reader["contactMidleName"].ToString();
                            addOrder.contactLastName = reader["contactLastName"].ToString();
                            addOrder.contactPhone = reader["contactPhone"].ToString();
                            addOrder.contactCompanyName = reader["contactCompanyName"].ToString();
                            addOrder.priceDetails = new contactPriceDetails();
                            addOrder.priceDetails.priceGroupName = reader["priceGroupName"].ToString();
                            addOrder.priceDetails.discountPersantage = reader.GetInt32(reader.GetOrdinal("priceGroupDiscountPercent"));
                            addOrder.orderReceiverId = reader.GetInt32(reader.GetOrdinal("orderReceiverId"));
                            addOrder.receiverCity = reader["receiverCity"].ToString();
                            addOrder.receiverAdress = reader["receiverAdress"].ToString();
                            addOrder.receiverFirstName = reader["receiverFirstName"].ToString();
                            addOrder.receiverMidleName = reader["receiverMidleName"].ToString();
                            addOrder.receiverLastName = reader["receiverLastName"].ToString();
                            addOrder.receiverPhone = reader["receiverPhone"].ToString();
                            addOrder.receiverCompanyName = reader["receiverCompanyName"].ToString();
                            addOrder.receiverPriceDetails = new contactPriceDetails();
                            addOrder.receiverPriceDetails.priceGroupName = reader["receiverPriceGroupName"].ToString();
                            addOrder.receiverPriceDetails.discountPersantage = reader.GetInt32(reader.GetOrdinal("receiverPriceGroupDiscountPercent"));
                            addOrder.orderTypeId = reader.GetInt32(reader.GetOrdinal("orderTypeId"));
                            addOrder.orderTypeName = reader["orderTypeName"].ToString();
                            addOrder.orderIsDocument = reader.GetBoolean(reader.GetOrdinal("orderIsDocument"));
                            addOrder.orderDescription = reader["orderDescription"].ToString();
                            addOrder.orderContent = reader["orderContent"].ToString();
                            addOrder.orderCount = reader.GetInt32(reader.GetOrdinal("orderCount"));
                            addOrder.orderWeight = reader.GetDouble(reader.GetOrdinal("orderWeight"));
                            addOrder.orderWidth = reader.GetDouble(reader.GetOrdinal("orderWidth"));
                            addOrder.orderHeight = reader.GetDouble(reader.GetOrdinal("orderHeight"));
                            addOrder.orderDepth = reader.GetDouble(reader.GetOrdinal("orderDepth"));
                            addOrder.chashOnDelivery = reader.GetBoolean(reader.GetOrdinal("chashOnDelivery"));
                            addOrder.chashOnDeliverySum = reader.GetDecimal(reader.GetOrdinal("chashOnDeliverySum"));
                            addOrder.orderInsuranceSum = (reader.GetValue(reader.GetOrdinal("orderInsuranceSum")) == DBNull.Value ? 0 : reader.GetDecimal(reader.GetOrdinal("orderInsuranceSum")));
                            addOrder.cashOnDeliveryTypeId = reader.GetInt32(reader.GetOrdinal("cashOnDeliveryTypeId"));
                            addOrder.chashOnDeliveryName = reader["chashOnDeliveryName"].ToString();
                            addOrder.orderDeliveryDateTime = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDateTime"));
                            addOrder.orderDeliveryDate = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDate"));
                            addOrder.orderDeliveryTime = reader.GetTimeSpan(reader.GetOrdinal("orderDeliveryTime"));
                            addOrder.orderCreatedDateTime = reader.GetDateTime(reader.GetOrdinal("orderCreatedDateTime"));
                            addOrder.orderCityPriceId = reader.GetInt32(reader.GetOrdinal("orderCityPriceId"));
                            addOrder.orderInterCityPriceId = reader.GetInt32(reader.GetOrdinal("orderInterCityPriceId"));
                            addOrder.orderInterCityPriceIdExpress = reader.GetBoolean(reader.GetOrdinal("orderInterCityPriceIdExpress"));
                            addOrder.orderExtraPricesId = reader["orderExtraPricesId"].ToString();
                            addOrder.orderPriceVat = reader.GetDecimal(reader.GetOrdinal("orderPriceVat"));
                            addOrder.orderPriceNoVat = reader.GetDecimal(reader.GetOrdinal("orderPriceNoVat"));
                            addOrder.cityDescription = reader["cityDescription"].ToString();
                            addOrder.interCityDescription = reader["interCityDescription"].ToString();
                            addOrder.deliveryType = reader["deliveryType"].ToString();
                            vReturnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }

                        vReturnList.resultState = true;
                        vReturnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnList.resultState = false;
                    vReturnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnList.resultState = false;
                    vReturnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return vReturnList;
        }

        [Authorize]
        public dataTableReturnClass getDataTableCounts(string connectionString, string whereClauseTotal, string whereClauseFilter, string searchDate, List<buildWhereClause> searchArrayObject)
        {
            dataTableReturnClass vResult = new dataTableReturnClass();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLStringTotalCount = "SELECT count(1) as returnData FROM [dbo].[vwMainViewOrder] " + whereClauseTotal;
                        string vSQLStringFiltered = "SELECT count(1) as returnData FROM [dbo].[vwMainViewOrder] " + whereClauseFilter;
                        var cmd = new SqlCommand(vSQLStringTotalCount, conn);
                        if (searchDate != "") { 
                            cmd.Parameters.AddWithValue("@searchDate", searchDate);
                        }
                        //cmd.Parameters.AddWithValue("@alwaysOne", 1);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vResult.totalRecords = reader.GetInt32(reader.GetOrdinal("returnData"));
                        }
                        var cmd1 = new SqlCommand(vSQLStringFiltered, conn);
                        if (searchDate != ""){
                            cmd1.Parameters.AddWithValue("@searchDate", searchDate);
                        }
                        //cmd1.Parameters.AddWithValue("@alwaysOne", 1);
                        for (int i = 0; i < searchArrayObject.Count; i++){
                            cmd1.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        reader = cmd1.ExecuteReader();
                        while (reader.Read()){
                            vResult.filteredRecords = reader.GetInt32(reader.GetOrdinal("returnData"));
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }

            return vResult;
        }

        private string getQueryFromCommand(SqlCommand cmd)
        {
            string CommandTxt = cmd.CommandText;


            foreach (SqlParameter parms in cmd.Parameters)
            {
                string val = String.Empty;
                if (parms.DbType.Equals(DbType.String) || parms.DbType.Equals(DbType.DateTime))
                    val = "'" + Convert.ToString(parms.Value).Replace(@"\", @"\\").Replace("'", @"\'") + "'";
                if (parms.DbType.Equals(DbType.Int16) || parms.DbType.Equals(DbType.Int32) || parms.DbType.Equals(DbType.Int64) || parms.DbType.Equals(DbType.Decimal) || parms.DbType.Equals(DbType.Double))
                    val = Convert.ToString(parms.Value);
                string paramname = parms.ParameterName;
                CommandTxt = CommandTxt.Replace("@"+paramname, val);
            }
            return (CommandTxt);
        }

        [Authorize]
        public returnStateWithOrdersMainView getOrderMainViewList(string userName, string whereClause, string searchDate, List<buildWhereClause> searchArrayObject, string sortBy, string vOffset, string pageLength, string connectionString)
        {
            returnStateWithOrdersMainView returnList = new returnStateWithOrdersMainView();
            returnList.returnObject = new List<OrdersMainView>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        //string vSQLString = "SELECT [autoId],[orderId],[CreatedBy],[stationName],[contactCity],[contactPhone],[contactCompanyName],[receiverCity],[receiverPhone],[receiverCompanyName],[orderTypeName],[orderIsDocument],[orderCount],[chashOnDelivery],[chashOnDeliverySum],[chashOnDeliveryName],[orderDeliveryDateTime],[orderDeliveryDate],[orderDeliveryTime],[orderCreatedDateTime] FROM [dbo].[vwMainViewOrder] " + whereClause + " " + sortBy + "OFFSET " + vOffset + " ROWS FETCH NEXT " + pageLength + " ROWS ONLY";
                        string vSQLString = "SELECT [orderId],[stationName], [contactPhone], [receiverCity], [receiverPhone], [receiverCompanyName], [orderTypeName], [chashOnDeliverySum],[orderInsuranceSum],[orderDeliveryDateTime],[orderDeliveryDate],[orderDeliveryTime],[orderCreatedDateTime],[orderDescriptionStationName],[statusDescription],[statusOrderFinished], [nearestStationName], [priorityOrder] FROM [dbo].[vwMainViewOrder] " + whereClause + " " + sortBy + " OFFSET " + vOffset + " ROWS FETCH NEXT " + pageLength + " ROWS ONLY";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@searchDate", searchDate);
                        //cmd.Parameters.AddWithValue("@alwaysOne", 1);
                        for (int i=0;i<searchArrayObject.Count;i++) {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue +"%");
                        };
                        var reader = cmd.ExecuteReader();
                        //It should give the final SQL. Add into watch if needed.
                        //getQueryFromCommand(cmd);
                        while (reader.Read())
                        {
                            var addOrder = new OrdersMainView();
                            //addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.orderId = reader["orderId"].ToString();
                            //addOrder.CreatedBy = reader["CreatedBy"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            //addOrder.contactCity = reader["contactCity"].ToString();
                            addOrder.contactPhone = reader["contactPhone"].ToString();
                            //addOrder.contactCompanyName = reader["contactCompanyName"].ToString();
                            addOrder.receiverCity = reader["receiverCity"].ToString();
                            addOrder.receiverPhone = reader["receiverPhone"].ToString();
                            addOrder.receiverCompanyName = reader["receiverCompanyName"].ToString();
                            addOrder.orderTypeName = reader["orderTypeName"].ToString();
                            //addOrder.orderIsDocument = reader.GetBoolean(reader.GetOrdinal("orderIsDocument"));
                            //addOrder.orderCount = reader.GetInt32(reader.GetOrdinal("orderCount"));
                            //addOrder.chashOnDelivery = reader.GetBoolean(reader.GetOrdinal("chashOnDelivery"));
                            addOrder.chashOnDeliverySum = reader.GetDecimal(reader.GetOrdinal("chashOnDeliverySum"));
                            addOrder.orderInsuranceSum = (reader.GetValue(reader.GetOrdinal("orderInsuranceSum")) == DBNull.Value ? 0 : reader.GetDecimal(reader.GetOrdinal("orderInsuranceSum")));
                            //addOrder.chashOnDeliveryName = reader["chashOnDeliveryName"].ToString();
                            addOrder.orderDeliveryDateTime = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDateTime"));
                            //addOrder.orderDeliveryDate = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDate"));
                            //addOrder.orderDeliveryTime = reader.GetTimeSpan(reader.GetOrdinal("orderDeliveryTime"));
                            addOrder.orderCreatedDateTime = reader.GetDateTime(reader.GetOrdinal("orderCreatedDateTime"));
                            addOrder.orderDescriptionStationName = reader["orderDescriptionStationName"].ToString();
                            addOrder.statusDescription = reader["statusDescription"].ToString();
                            addOrder.statusOrderFinished = reader.GetBoolean(reader.GetOrdinal("statusOrderFinished"));
                            addOrder.nearestStationName = reader["nearestStationName"].ToString();
                            addOrder.priorityOrder = reader.GetInt32(reader.GetOrdinal("priorityOrder"));
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
                        returnList.filteredCount = (int)com.ExecuteScalar();
                        returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

        [Authorize]
        public returnStateWithOrdersMainView getOrderMainViewListNearestStation(string userName, string whereClause, List<buildWhereClause> searchArrayObject, string sortBy, string vOffset, string pageLength, string connectionString)
        {
            returnStateWithOrdersMainView returnList = new returnStateWithOrdersMainView();
            returnList.returnObject = new List<OrdersMainView>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "SELECT [orderId],[stationName], [contactCity], [contactAddress], [contactPhone], [senderNearestStationName], [receiverCity], [receiverPhone], [receiverCompanyName], [receiverAddress], [orderTypeName], [chashOnDeliverySum],[orderInsuranceSum],[orderDeliveryDateTime],[orderDeliveryDate],[orderDeliveryTime],[orderCreatedDateTime],[orderDescriptionStationName],[statusDescription],[statusOrderFinished],[priorityOrder], [courierLoginName] FROM [dbo].[vwMainViewOrder] " + whereClause + " " + sortBy + " OFFSET " + vOffset + " ROWS FETCH NEXT " + pageLength + " ROWS ONLY";
                        var cmd = new SqlCommand(vSQLString, conn);
                        //cmd.Parameters.AddWithValue("@alwaysOne", 1);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        var reader = cmd.ExecuteReader();
                        getQueryFromCommand(cmd);
                        while (reader.Read())
                        {
                            var addOrder = new OrdersMainView();
                            //addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.orderId = reader["orderId"].ToString();
                            //addOrder.CreatedBy = reader["CreatedBy"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            addOrder.contactCity = reader["contactCity"].ToString();
                            addOrder.contactAddress = reader["contactAddress"].ToString();
                            addOrder.contactPhone = reader["contactPhone"].ToString();
                            addOrder.senderNearestStationName = reader["senderNearestStationName"].ToString();
                            addOrder.receiverCity = reader["receiverCity"].ToString();
                            addOrder.receiverPhone = reader["receiverPhone"].ToString();
                            addOrder.receiverCompanyName = reader["receiverCompanyName"].ToString();
                            addOrder.receiverAddress = reader["receiverAddress"].ToString();
                            addOrder.orderTypeName = reader["orderTypeName"].ToString();
                            //addOrder.orderCount = reader.GetInt32(reader.GetOrdinal("orderCount"));
                            //addOrder.chashOnDelivery = reader.GetBoolean(reader.GetOrdinal("chashOnDelivery"));
                            addOrder.chashOnDeliverySum = reader.GetDecimal(reader.GetOrdinal("chashOnDeliverySum"));
                            addOrder.orderInsuranceSum = (reader.GetValue(reader.GetOrdinal("orderInsuranceSum")) == DBNull.Value ? 0 : reader.GetDecimal(reader.GetOrdinal("orderInsuranceSum")));
                            //addOrder.chashOnDeliveryName = reader["chashOnDeliveryName"].ToString();
                            addOrder.orderDeliveryDateTime = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDateTime"));
                            //addOrder.orderDeliveryDate = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDate"));
                            //addOrder.orderDeliveryTime = reader.GetTimeSpan(reader.GetOrdinal("orderDeliveryTime"));
                            addOrder.orderCreatedDateTime = reader.GetDateTime(reader.GetOrdinal("orderCreatedDateTime"));
                            addOrder.orderDescriptionStationName = reader["orderDescriptionStationName"].ToString();
                            addOrder.statusDescription = reader["statusDescription"].ToString();
                            addOrder.statusOrderFinished = reader.GetBoolean(reader.GetOrdinal("statusOrderFinished"));
                            addOrder.priorityOrder = reader.GetInt32(reader.GetOrdinal("priorityOrder")); 
                            addOrder.courierLoginName = reader["courierLoginName"].ToString();
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
                        returnList.filteredCount = (int)com.ExecuteScalar();
                        returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

        //It's not used anywere commeing out 
        /*
        [Authorize]
        public returnStateWithOrdersMainView getOrderMainViewList_static(string userName, string whereClause, string connectionString)
        {
            returnStateWithOrdersMainView returnList = new returnStateWithOrdersMainView();
            returnList.returnObject = new List<OrdersMainView>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "SELECT [autoId],[orderId],[CreatedBy],[stationName],[contactCity],[contactPhone],[contactCompanyName],[receiverCity],[receiverPhone],[receiverCompanyName],[orderTypeName],[orderIsDocument],[orderCount],[chashOnDelivery],[chashOnDeliverySum],[orderInsuranceSum],[orderDeliveryDateTime],[orderDeliveryDate],[orderDeliveryTime],[orderCreatedDateTime] FROM [dbo].[vwMainViewOrder] " + whereClause + " order by [orderId]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addOrder = new OrdersMainView();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.orderId = reader["orderId"].ToString();
                            addOrder.CreatedBy = reader["CreatedBy"].ToString();
                            addOrder.stationName = reader["stationName"].ToString();
                            addOrder.contactCity = reader["contactCity"].ToString();
                            addOrder.contactPhone = reader["contactPhone"].ToString();
                            addOrder.contactCompanyName = reader["contactCompanyName"].ToString();
                            addOrder.receiverCity = reader["receiverCity"].ToString();
                            addOrder.receiverPhone = reader["receiverPhone"].ToString();
                            addOrder.receiverCompanyName = reader["receiverCompanyName"].ToString();
                            addOrder.orderTypeName = reader["orderTypeName"].ToString();
                            addOrder.orderIsDocument = reader.GetBoolean(reader.GetOrdinal("orderIsDocument"));
                            addOrder.orderCount = reader.GetInt32(reader.GetOrdinal("orderCount"));
                            addOrder.chashOnDelivery = reader.GetBoolean(reader.GetOrdinal("chashOnDelivery"));
                            addOrder.chashOnDeliverySum = reader.GetDecimal(reader.GetOrdinal("chashOnDeliverySum"));
                            addOrder.orderInsuranceSum = (reader.GetValue(reader.GetOrdinal("orderInsuranceSum")) == DBNull.Value ? 0 : reader.GetDecimal(reader.GetOrdinal("orderInsuranceSum")));
                            //addOrder.chashOnDeliveryName = reader["chashOnDeliveryName"].ToString();
                            addOrder.orderDeliveryDateTime = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDateTime"));
                            addOrder.orderDeliveryDate = reader.GetDateTime(reader.GetOrdinal("orderDeliveryDate"));
                            addOrder.orderDeliveryTime = reader.GetTimeSpan(reader.GetOrdinal("orderDeliveryTime"));
                            addOrder.orderCreatedDateTime = reader.GetDateTime(reader.GetOrdinal("orderCreatedDateTime"));
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
                        returnList.filteredCount = (int)com.ExecuteScalar();
                        returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }
        */

        [Authorize]
        public returnStateWithList getContactList(string userName, string connectionString, string whereClause, List<buildWhereClause> searchArrayObject, string sortBy, string vOffset, string rowLength)
        {
            returnStateWithList returnList = new returnStateWithList();
            returnList.returnObject = new List<Contacts>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId], [contactCompanyName], [contactFirstName], [contactLastName], [contactCity], [contactAddress], [contactPhone], [priceGroupName], [priceGroupDiscountPercent], [conctactNearestStationName] FROM [dbo].[vwContacts] where contactEnabled = 1 and contactDeleted = 0 " + whereClause + " " + sortBy + "OFFSET " + vOffset + " ROWS FETCH NEXT " + rowLength + " ROWS ONLY";
                        var cmd = new SqlCommand(vSQLString, conn);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addOrder = new Contacts();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.contactCompanyName = reader["contactCompanyName"].ToString();
                            addOrder.contactFirstName = reader["contactFirstName"].ToString();
                            addOrder.contactLastName = reader["contactLastName"].ToString();
                            addOrder.contactCity = reader["contactCity"].ToString();
                            addOrder.contactAddress = reader["contactAddress"].ToString();
                            addOrder.contactPhone = reader["contactPhone"].ToString();
                            addOrder.priceGroupName = reader["priceGroupName"].ToString();
                            addOrder.priceDetails = new contactPriceDetails();
                            addOrder.priceDetails.priceGroupName = reader["priceGroupName"].ToString();
                            addOrder.priceDetails.discountPersantage = reader.GetInt32(reader.GetOrdinal("priceGroupDiscountPercent"));
                            addOrder.conctactNearestStationName = reader["conctactNearestStationName"].ToString(); 
                            returnList.returnObject.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
                        returnList.filteredCount = (int)com.ExecuteScalar();

                        com.CommandText = "select count(1) from [dbo].[contacts] with (nolock) where contactEnabled = 1 and contactDeleted = 0";
                        returnList.totalCount = (int)com.ExecuteScalar();
                        
                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";

                    }
                }
                catch (SqlException ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

        [Authorize]
        public returnStateWithList getReceiverList(string userName, string connectionString, string whereClause, List<buildWhereClause> searchArrayObject, string sortBy, string vOffset)
        {
            returnStateWithList returnList = new returnStateWithList();
            returnList.returnObjectReceiver = new List<Receiver>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[contactCompanyName],[contactFirstName],[contactLastName],[contactCity],[contactAddress],[contactPhone], [priceGroupName], [priceGroupDiscountPercent], [conctactNearestStationName]  FROM [dbo].[vwContacts] where contactEnabled = 1 and contactDeleted = 0 " + whereClause + " " + sortBy + "OFFSET " + vOffset + " ROWS FETCH NEXT 10 ROWS ONLY";
                        var cmd = new SqlCommand(vSQLString, conn);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addOrder = new Receiver();
                            addOrder.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addOrder.receiverCompanyName = reader["contactCompanyName"].ToString();
                            addOrder.receiverFirstName = reader["contactFirstName"].ToString();
                            addOrder.receiverLastName = reader["contactLastName"].ToString();
                            addOrder.receiverCity = reader["contactCity"].ToString();
                            addOrder.receiverAdress = reader["contactAddress"].ToString();
                            addOrder.receiverPhone = reader["contactPhone"].ToString();
                            addOrder.receiverPriceGroupName = reader["priceGroupName"].ToString();
                            addOrder.receiverPriceDetails = new contactPriceDetails();
                            addOrder.receiverPriceDetails.priceGroupName = reader["priceGroupName"].ToString();
                            addOrder.receiverPriceDetails.discountPersantage = reader.GetInt32(reader.GetOrdinal("priceGroupDiscountPercent"));
                            addOrder.receiverConctactNearestStationName = reader["conctactNearestStationName"].ToString();

                            returnList.returnObjectReceiver.Add(addOrder);
                            addOrder = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
                        returnList.filteredCount = (int)com.ExecuteScalar();

                        com.CommandText = "select count(1) from [dbo].[contacts] with (nolock) where contactEnabled = 1 and contactDeleted = 0";
                        returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";

                    }
                }
                catch (SqlException ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = true;
                    returnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

		//########################################## Extra pricess begin ##########################################

		[Authorize]
		public class deliveryExtraServices
		{
			public int autoId { get; set; }
			public string description { get; set; }
			public bool priceIsPersentage { get; set; }
            public string persentageCalcField { get; set; }
            public decimal priceNoVat { get; set; }
			public decimal priceVat { get; set; }
		}

		[Authorize]
		public class interCityDeliveryPrices
		{
			public int autoId { get; set; }
			public string description { get; set; }
			public double weightLimit { get; set; }
			public decimal expressPriceNoVat { get; set; }
			public decimal expressPriceVat { get; set; }
			public decimal economicalPriceNoVat { get; set; }
			public decimal economicalPriceVat { get; set; }
		}

		[Authorize]
		public class cityDeliveryPrices
		{
			public int autoId { get; set; }
			public string description { get; set; }
            public int deliveryTimeMinute { get; set; }
            public decimal priceNoVat { get; set; }
			public decimal priceVat { get; set; }
		}

		public class returnStatePriceLists
		{
			public bool resultState { get; set; }
			public string resultMessage { get; set; }
			public int totalCount { get; set; }
			public List<cityDeliveryPrices> cityDeliveryList = null;
			public List<deliveryExtraServices> deliveryExtraServicesList = null;
			public List<interCityDeliveryPrices> interCityDeliveryPricesList = null;
		}

        public class returnStateCalculatePrice
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public decimal priceNoVat { get; set; }
            public decimal priceVat { get; set; }
        }

        [Authorize]
        public returnStatePriceLists getInterCityPricesList(string userName, string connectionString)
        {
			returnStatePriceLists returnList = new returnStatePriceLists();
            returnList.interCityDeliveryPricesList = new List<interCityDeliveryPrices>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId], [description], [weightLimit], [expressPriceNoVat], [expressPriceVat], [economicalPriceNoVat], [economicalPriceVat] FROM [dbo].[interCityPrices] where enabled = 1 and deleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addPrice = new interCityDeliveryPrices();
							addPrice.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
							addPrice.description = reader["description"].ToString();
							addPrice.weightLimit = reader.GetDouble(reader.GetOrdinal("weightLimit"));
							addPrice.expressPriceNoVat = reader.GetDecimal(reader.GetOrdinal("expressPriceNoVat"));
							addPrice.expressPriceVat = reader.GetDecimal(reader.GetOrdinal("expressPriceVat"));
							addPrice.economicalPriceNoVat = reader.GetDecimal(reader.GetOrdinal("economicalPriceNoVat"));
							addPrice.economicalPriceVat = reader.GetDecimal(reader.GetOrdinal("economicalPriceVat"));
                            returnList.interCityDeliveryPricesList.Add(addPrice);
							addPrice = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
						returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

		[Authorize]
        public returnStatePriceLists getExtraServicesList(string userName, string connectionString, string extraWhereCondition = "")
        {
			returnStatePriceLists returnList = new returnStatePriceLists();
            returnList.deliveryExtraServicesList = new List<deliveryExtraServices>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId], [description], [priceIsPersentage], [persentageCalcField], [priceNoVat], [priceVat] FROM [dbo].[extraServicesPrices] where enabled = 1 and deleted = 0 " + ( extraWhereCondition.Length > 0 ? " and " + extraWhereCondition : "" );
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addPrice = new deliveryExtraServices();
							addPrice.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
							addPrice.description = reader["description"].ToString();
							addPrice.priceIsPersentage = reader.GetBoolean(reader.GetOrdinal("priceIsPersentage"));
                            addPrice.persentageCalcField = reader["persentageCalcField"].ToString();
                            addPrice.priceNoVat = reader.GetDecimal(reader.GetOrdinal("priceNoVat"));
							addPrice.priceVat = reader.GetDecimal(reader.GetOrdinal("priceVat"));
                            returnList.deliveryExtraServicesList.Add(addPrice);
                            addPrice = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
						returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

		[Authorize]
        public returnStatePriceLists getCityPricessList(string userName, string connectionString)
        {
			returnStatePriceLists returnList = new returnStatePriceLists();
            returnList.cityDeliveryList = new List<cityDeliveryPrices>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId], [description], [deliveryTimeMinute], [priceNoVat], [priceVat] FROM [dbo].[cityDeliveryPrices] where enabled = 1 and deleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addPrice = new cityDeliveryPrices();
							addPrice.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
							addPrice.description = reader["description"].ToString();
                            addPrice.deliveryTimeMinute = (reader.GetValue(reader.GetOrdinal("deliveryTimeMinute")) == DBNull.Value ? 0 : reader.GetInt32(reader.GetOrdinal("deliveryTimeMinute")) );
                            addPrice.priceNoVat = reader.GetDecimal(reader.GetOrdinal("priceNoVat"));
							addPrice.priceVat = reader.GetDecimal(reader.GetOrdinal("priceVat"));
                            returnList.cityDeliveryList.Add(addPrice);
                            addPrice = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";
						returnList.totalCount = (int)com.ExecuteScalar();

                        returnList.resultState = true;
                        returnList.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

        //Calculate price by given parameters
        [Authorize]
        public returnStateCalculatePrice calculatePrice (newOrderClass orderDetails, string userName, string connectionString) {
            returnStateCalculatePrice functionReturn = new returnStateCalculatePrice();
            decimal vPriceVat = 0;
            decimal vPriceNoVat = 0;
            string fieldNamesVat = "priceVat";
            string fieldNamesNoVat = "priceNoVat";
            var userModel = new UsersViewModel();

            //Case when order is interCity
            if (orderDetails.orderInterCityPriceId > 0) {
                fieldNamesVat = (orderDetails.orderInterCityPriceIdExpress ? "expressPriceVat" : "economicalPriceVat");
                fieldNamesNoVat = (orderDetails.orderInterCityPriceIdExpress ? "expressPriceNoVat" : "economicalPriceNoVat");
                //Get price for the actual weight if the weight is < 10 kg.:
                if (orderDetails.orderWeight <= 10) { 
                    vPriceVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[interCityPrices]", "( " + fieldNamesVat + " * " + Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + " )", "enabled = 1 and deleted = 0 and weightLimit >= cast(" + Convert.ToString( orderDetails.orderWeight).Replace(",", ".") + " as int) and weightLimit < cast( " + Convert.ToString(orderDetails.orderWeight ).Replace(",", ".") + "+1 as int)", connectionString));
                    vPriceNoVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[interCityPrices]", "( " + fieldNamesNoVat + " * " + Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + " )", "enabled = 1 and deleted = 0 and weightLimit >= cast(" + Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + " as int) and weightLimit < cast( " + Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + "+1 as int)", connectionString));
                }
                if (orderDetails.orderWeight > 10){
                    vPriceVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[interCityPrices]", "( (select " + fieldNamesVat + " from[dbo].[interCityPrices] where weightLimit = 10) * 10 + ( " + fieldNamesVat + " * ( "+ Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + "-10 ) ) )", "enabled = 1 and deleted = 0 and weightLimit > 10", connectionString));
                    vPriceNoVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[interCityPrices]", "( (select " + fieldNamesNoVat + " from[dbo].[interCityPrices] where weightLimit = 10) * 10 + ( " + fieldNamesNoVat + " * ( " + Convert.ToString(orderDetails.orderWeight).Replace(",", ".") + "-10 ) ) )", "enabled = 1 and deleted = 0 and weightLimit > 10", connectionString));
                }
            }
            //Case when order is interCity
            if (orderDetails.orderCityPriceId > 0){
                vPriceVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[cityDeliveryPrices]", fieldNamesVat, "enabled = 1 and deleted = 0 and autoId = " + Convert.ToString(orderDetails.orderCityPriceId), connectionString));
                vPriceNoVat = Convert.ToDecimal(userModel.getFieldByTableAndConditionSafe("[dbo].[cityDeliveryPrices]", fieldNamesNoVat, "enabled = 1 and deleted = 0 and autoId = " + Convert.ToString(orderDetails.orderCityPriceId), connectionString));
            }

            //Calculate extra pricess
            if (orderDetails.orderExtraPricesId.Length > 0) { 
                returnStatePriceLists allExtraServices = getExtraServicesList(userName, connectionString, "autoId in ( " + orderDetails.orderExtraPricesId + " ) ");
                foreach (var extraPriceObject in allExtraServices.deliveryExtraServicesList) {
                    if ( extraPriceObject.priceIsPersentage) {
                        PropertyInfo propertyInfo = orderDetails.GetType().GetProperty(extraPriceObject.persentageCalcField);
                        decimal toCalc = (decimal)propertyInfo.GetValue(orderDetails);
                        vPriceVat = vPriceVat + calulatePersantage(extraPriceObject.priceVat, toCalc);
                        vPriceNoVat = vPriceNoVat + calulatePersantage(extraPriceObject.priceNoVat, toCalc);
                    }

                    if (!extraPriceObject.priceIsPersentage){
                        vPriceVat = vPriceVat + extraPriceObject.priceVat;
                        vPriceNoVat = vPriceNoVat + extraPriceObject.priceNoVat;
                    }
                }
            }

            functionReturn.resultState = true;
            functionReturn.resultMessage = "Sucess";
            functionReturn.priceVat = vPriceVat;
            functionReturn.priceNoVat = vPriceNoVat;

            return functionReturn;
        }

        [Authorize]
        public decimal calulatePersantage(decimal vPercentage, decimal vValue){
            decimal vResult = 0;
            vResult = (vPercentage * vValue) / 100;
            return vResult;
        }
        //########################################## Extra pricess end ##########################################

        //Lock order functions
        public int lockOrder( string orderId, string loginName, string connectionString){
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "update [dbo].[orders] set orderLocked = 1, orderLockedByUserName = '"+ loginName + "' where orderId = orderId";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex){
                    return -1;
                }
                catch (Exception ex){
                    return -1;
                }
                finally
                {
                    conn.Close();
                }
            }
            return 0;
        }

        public int unLockOrder( string orderId, string loginName, string connectionString){
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "update [dbo].[orders] set orderLocked = 0, orderLockedByUserName = '' where orderId = orderId";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return 0;
        }
        //Lock order functions end

        public returnState assignCourier(string loggedUser, string orderId, string courierId, string connectionSting){
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[setCourierToOrder]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@courierId", SqlDbType.Int).Value = courierId;
                        cmd.Parameters.Add("@orderId", SqlDbType.NVarChar).Value = orderId;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                        vReturnState.resultOrderId = "1";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnState;
        }
    }
}
