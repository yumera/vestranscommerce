﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;

namespace startProject1.Models
{
    public class PricesViewModel : Controller
    {
        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        [Authorize]
        public class weightClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        [Authorize]
        public List<weightClass> htmlFillWeights()
        {
            List<weightClass> vReturnSelect = new List<weightClass>();

            int vMaxCount = 15;
            int vCurrentCount = 0;

            while (vCurrentCount <= vMaxCount) {
                vCurrentCount++;
                vReturnSelect.Add(new weightClass
                {
                    key = vCurrentCount.ToString(),
                    selectLabel = vCurrentCount.ToString()
                });
            }

            vReturnSelect.Add(new weightClass
            {
                key = "999999999",
                selectLabel = "999999999"
            });

            return vReturnSelect;
        }


        [Display(Name = "Описание: ")]
        [Required(ErrorMessage = "Моля попълнете описание", AllowEmptyStrings = false)]
        public string descriptionCityPrice { get; set; }

        [Display(Name = "Доставка минути:")]
        //[Required(ErrorMessage = "Моля попълнете доставка минути", AllowEmptyStrings = true)]
        public int deliveryTimeMinuteCityPrice { get; set; }

        [Display(Name = "Цена без ДДС:")]
        [Required(ErrorMessage = "Моля попълнете цена без ДДС", AllowEmptyStrings = false)]
        public decimal priceNoVatCityPrice { get; set; }

        [Display(Name = "Цена със ДДС:")]
        [Required(ErrorMessage = "Моля попълнете цена със ДДС", AllowEmptyStrings = false)]
        public decimal priceVatCityPrice { get; set; }


        [Display(Name = "Описание: ")]
        [Required(ErrorMessage = "Моля попълнете описание", AllowEmptyStrings = false)]
        public string descriptionInterCityPrice { get; set; }

        [Display(Name = "Лимит тегло:")]
        [Required(ErrorMessage = "Моля попълнете доставка минути", AllowEmptyStrings = false)]
        public SelectList weightLimitInterCityPrice { get; set; }

        [Display(Name = "Експресна цена без ДДС:")]
        [Required(ErrorMessage = "Моля попълнете експресна цена без ДДС", AllowEmptyStrings = false)]
        public decimal expressPriceNoVatInterCityPrice { get; set; }

        [Display(Name = "Експресна цена със ДДС:")]
        [Required(ErrorMessage = "Моля попълнете експресна цена със ДДС", AllowEmptyStrings = false)]
        public decimal expressPriceVatInterCityPrice { get; set; }

        [Display(Name = "Икономична цена без ДДС:")]
        [Required(ErrorMessage = "Моля попълнете икономична цена без ДДС", AllowEmptyStrings = false)]
        public decimal economicalPriceNoVatInterCityPrice { get; set; }

        [Display(Name = "Икономична цена със ДДС:")]
        [Required(ErrorMessage = "Моля попълнете икономична цена със ДДС", AllowEmptyStrings = false)]
        public decimal economicalPriceVatInterCityPrice { get; set; }

        public class CityPricesViewModel : Controller
        {
            [Authorize]
            public class CityPrices
            {
                public int autoId { get; set; }
                public string description { get; set; }
                public int deliveryTimeMinute { get; set; }
                public decimal priceNoVat { get; set; }
                public decimal priceVat { get; set; }
                public bool enabled { get; set; }
                public bool deleted { get; set; }
            }

            public class returnStateWithCityList
            {
                public bool resultState { get; set; }
                public string resultMessage { get; set; }
                public int totalCount { get; set; }
                public List<CityPrices> returnObject;
            }


            [Authorize]
            public returnState saveCityPrice(CityPrices newCityPrice, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[createCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = newCityPrice.description;
                            cmd.Parameters.Add("@deliveryTimeMinute", SqlDbType.Int).Value = newCityPrice.deliveryTimeMinute;
                            cmd.Parameters.Add("@priceNoVat", SqlDbType.Decimal).Value = newCityPrice.priceNoVat;
                            cmd.Parameters.Add("@priceVat", SqlDbType.Decimal).Value = newCityPrice.priceVat;

                            var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                            returnParameter.Direction = ParameterDirection.ReturnValue;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = ((int)returnParameter.Value > 0);

                            vReturnState.resultState = true;
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex){
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex){
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally{
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnState deleteCityPrice(int vCityPriceAutoId, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[deleteCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@cityPrice_autoId", SqlDbType.Int).Value = vCityPriceAutoId;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = true;
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex){
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex){
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally{
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnState editCityPrice(CityPrices existingCityPrice, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[editCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = existingCityPrice.description;
                            cmd.Parameters.Add("@deliveryTimeMinute", SqlDbType.Int).Value = existingCityPrice.deliveryTimeMinute;
                            cmd.Parameters.Add("@priceNoVat", SqlDbType.Decimal).Value = existingCityPrice.priceNoVat;
                            cmd.Parameters.Add("@priceVat", SqlDbType.Decimal).Value = existingCityPrice.priceVat;
                            cmd.Parameters.Add("@selectedAutoId", SqlDbType.Int).Value = existingCityPrice.autoId;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = true;
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnStateWithCityList getCityPricesList(string userName, string connectionString)
            {
                returnStateWithCityList returnList = new returnStateWithCityList();
                returnList.returnObject = new List<CityPrices>();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionString;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            string vSQLString = "SELECT [autoId], [description], [deliveryTimeMinute], [priceNoVat], [priceVat] FROM [dbo].[cityDeliveryPrices] where [enabled] = 1 and [deleted] = 0";
                            var cmd = new SqlCommand(vSQLString, conn);
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                var addCityPrices = new CityPrices();
                                addCityPrices.autoId               = reader.GetInt32(reader.GetOrdinal("autoId"));
                                addCityPrices.description          = reader["description"].ToString();
                                addCityPrices.deliveryTimeMinute   = (reader.GetValue(reader.GetOrdinal("deliveryTimeMinute")) == DBNull.Value ? 0 : reader.GetInt32(reader.GetOrdinal("deliveryTimeMinute"))); 
                                addCityPrices.priceNoVat           = reader.GetDecimal(reader.GetOrdinal("priceNoVat"));
                                addCityPrices.priceVat             = reader.GetDecimal(reader.GetOrdinal("priceVat"));
                                returnList.returnObject.Add(addCityPrices);
                                addCityPrices = null;
                            }
                            var com = conn.CreateCommand();
                            com.CommandText = "select @@ROWCOUNT";

                            returnList.totalCount = (int)com.ExecuteScalar();
                            returnList.resultState = true;
                            returnList.resultMessage = "List succesfully";

                        }
                    }
                    catch (SqlException ex){
                        returnList.resultState = false;
                        returnList.resultMessage = ex.Message;
                    }
                    catch (Exception ex){
                        returnList.resultState = false;
                        returnList.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                return returnList;
            }

        }

        public class InterCityPricesViewModel : Controller
        {

            [Authorize]
            public class InterCityPrices
            {
                public int autoId { get; set; }
                public string description { get; set; }
                public double weightLimit { get; set; }
                public decimal expressPriceNoVat { get; set; }
                public decimal expressPriceVat { get; set; }
                public decimal economicalPriceNoVat { get; set; }
                public decimal economicalPriceVat { get; set; }
                public bool enabled { get; set; }
                public bool deleted { get; set; }
            }

            [Authorize]
            public class returnStateWithInterCityList
            {
                public bool resultState { get; set; }
                public string resultMessage { get; set; }
                public int totalCount { get; set; }
                public List<InterCityPrices> returnObject;
            }

            [Authorize]
            public returnState saveInterCityPrice(InterCityPrices newInterCityPrice, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[createInterCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = newInterCityPrice.description;
                            cmd.Parameters.Add("@weightLimit", SqlDbType.Float).Value = newInterCityPrice.weightLimit;
                            cmd.Parameters.Add("@expressPriceNoVat", SqlDbType.Decimal).Value = newInterCityPrice.expressPriceNoVat;
                            cmd.Parameters.Add("@expressPriceVat", SqlDbType.Decimal).Value = newInterCityPrice.expressPriceVat;
                            cmd.Parameters.Add("@economicalPriceNoVat", SqlDbType.Decimal).Value = newInterCityPrice.economicalPriceNoVat;
                            cmd.Parameters.Add("@economicalPriceVat", SqlDbType.Decimal).Value = newInterCityPrice.economicalPriceVat;

                            var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                            returnParameter.Direction = ParameterDirection.ReturnValue;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = ((int)returnParameter.Value > 0);
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnState editInterCityPrice(InterCityPrices existingInterCityPrice, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[editInterCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = existingInterCityPrice.description;
                            cmd.Parameters.Add("@weightLimit", SqlDbType.Float).Value = existingInterCityPrice.weightLimit;
                            cmd.Parameters.Add("@expressPriceNoVat", SqlDbType.Decimal).Value = existingInterCityPrice.expressPriceNoVat;
                            cmd.Parameters.Add("@expressPriceVat", SqlDbType.Decimal).Value = existingInterCityPrice.expressPriceVat;
                            cmd.Parameters.Add("@economicalPriceNoVat", SqlDbType.Decimal).Value = existingInterCityPrice.economicalPriceNoVat;
                            cmd.Parameters.Add("@economicalPriceVat", SqlDbType.Decimal).Value = existingInterCityPrice.economicalPriceVat;
                            cmd.Parameters.Add("@selectedAutoId", SqlDbType.Int).Value = existingInterCityPrice.autoId;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = true;
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnState deleteInterCityPrice(int vInterCityPriceAutoId, string connectionSting)
            {
                returnState vReturnState = new returnState();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionSting;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            var cmd = new SqlCommand("[dbo].[deleteInterCityPrice]", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@interCityPrice_autoId", SqlDbType.Int).Value = vInterCityPriceAutoId;

                            cmd.ExecuteNonQuery();
                            vReturnState.resultState = true;
                            vReturnState.resultMessage = "Saved succesfully";
                        }
                    }
                    catch (SqlException ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        vReturnState.resultState = false;
                        vReturnState.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }

                }
                return vReturnState;
            }

            [Authorize]
            public returnStateWithInterCityList getInterCityPricesList(string userName, string connectionString)
            {
                returnStateWithInterCityList returnList = new returnStateWithInterCityList();
                returnList.returnObject = new List<InterCityPrices>();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = connectionString;
                    try
                    {
                        conn.Open();
                        if (conn.State == ConnectionState.Open)
                        {
                            string vSQLString = "SELECT [autoId], [description], [weightLimit], [expressPriceNoVat], [expressPriceVat], [economicalPriceNoVat], [economicalPriceVat] FROM [dbo].[interCityPrices] where [enabled] = 1 and [deleted] = 0";
                            var cmd = new SqlCommand(vSQLString, conn);
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                var addInterCityPrices = new InterCityPrices();
                                addInterCityPrices.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                                addInterCityPrices.description = reader["description"].ToString();
                                addInterCityPrices.weightLimit = reader.GetDouble(reader.GetOrdinal("weightLimit"));
                                addInterCityPrices.expressPriceNoVat = reader.GetDecimal(reader.GetOrdinal("expressPriceNoVat"));
                                addInterCityPrices.expressPriceVat = reader.GetDecimal(reader.GetOrdinal("expressPriceVat"));
                                addInterCityPrices.economicalPriceNoVat = reader.GetDecimal(reader.GetOrdinal("economicalPriceNoVat"));
                                addInterCityPrices.economicalPriceVat = reader.GetDecimal(reader.GetOrdinal("economicalPriceVat"));
                                returnList.returnObject.Add(addInterCityPrices);
                                addInterCityPrices = null;
                            }
                            var com = conn.CreateCommand();
                            com.CommandText = "select @@ROWCOUNT";

                            returnList.totalCount = (int)com.ExecuteScalar();
                            returnList.resultState = true;
                            returnList.resultMessage = "List succesfully";

                        }
                    }
                    catch (SqlException ex)
                    {
                        returnList.resultState = false;
                        returnList.resultMessage = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        returnList.resultState = false;
                        returnList.resultMessage = ex.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                return returnList;
            }


        }

    }
}