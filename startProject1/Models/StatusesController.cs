﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace startProject1.Models
{
    public class StatusesController : Controller
    {
        [Display(Name = "Статус име")]
        [Required(ErrorMessage = "Статус е задължителен", AllowEmptyStrings = false)]
        public string statusType { get; set; }

        [Display(Name = "Статус описание")]
        [Required(ErrorMessage = "Описание е задължителено", AllowEmptyStrings = false)]
        public string statusDescription { get; set; }

        [Display(Name = "Поръчката е завършена")]
        public bool statusOrderFinished { get; set; }

        [Authorize]
        public class statusesView
        {
            public int autoId { get; set; }
            public string statusType { get; set; }
            public string statusDescription { get; set; }
            public DateTime statusCreationDT { get; set; }
            public string loginName { get; set; }
            public bool statusOrderFinished { get; set; }
        }

        [Authorize]
        public class statuses
        {
            public int autoId { get; set; }
            public string statusType { get; set; }
            public string statusDescription { get; set; }
            public int statusCreateByUserId { get; set; }
            public bool statusOrderFinished { get; set; }
        }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        public class returnStateWithList
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public int totalCount { get; set; }
            public List<statusesView> returnObject;
        }

        [Authorize]
        public returnState createStatus(statuses newStatus, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[createStatuses]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@statusType", SqlDbType.VarChar).Value = newStatus.statusType;
                        cmd.Parameters.Add("@statusDescription", SqlDbType.VarChar).Value = newStatus.statusDescription;
                        cmd.Parameters.Add("@statusCreateByUserId", SqlDbType.Int).Value = newStatus.statusCreateByUserId;
                        cmd.Parameters.Add("@statusOrderFinished", SqlDbType.Int).Value = newStatus.statusOrderFinished; 

                        var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState editStatus(statuses editStatus, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[editStatuses]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@statusType", SqlDbType.VarChar).Value = editStatus.statusType;
                        cmd.Parameters.Add("@statusDescription", SqlDbType.VarChar).Value = editStatus.statusDescription;
                        cmd.Parameters.Add("@selectedAutoId", SqlDbType.Int).Value = editStatus.autoId;
                        cmd.Parameters.Add("@statusOrderFinished", SqlDbType.Int).Value = editStatus.statusOrderFinished;

                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState deleteStatus(statuses editStatus, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[deleteStatuses]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@selectedAutoId", SqlDbType.Int).Value = editStatus.autoId;

                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnStateWithList geStatuses(string userName, string connectionString)
        {
            returnStateWithList returnList = new returnStateWithList();
            returnList.returnObject = new List<statusesView>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[statusType],[statusDescription],[statusCreationDT],[statusOrderFinished],[loginName] FROM [dbo].[vwStatuses]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            var addStatuses = new statusesView();
                            addStatuses.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addStatuses.statusType = reader["statusType"].ToString();
                            addStatuses.statusDescription = reader["statusDescription"].ToString();
                            addStatuses.statusCreationDT = reader.GetDateTime(reader.GetOrdinal("statusCreationDT"));
                            addStatuses.statusOrderFinished = reader.GetBoolean(reader.GetOrdinal("statusOrderFinished"));
                            addStatuses.loginName = reader["loginName"].ToString();
                            returnList.returnObject.Add(addStatuses);
                            addStatuses = null;
                        }
                        var com = conn.CreateCommand();
                        com.CommandText = "select @@ROWCOUNT";

                        returnList.totalCount = (int)com.ExecuteScalar();
                        returnList.resultState = true;
                        returnList.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnList.resultState = false;
                    returnList.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

    }
}