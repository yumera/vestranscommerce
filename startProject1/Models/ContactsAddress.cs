﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static startProject1.Models.OrderFormModel;

namespace startProject1.Models
{
    public class ContactsAddress : Controller
    {
        [Display(Name = "Фирма име")]
        public string contactCompanyName { get; set; }

        [Display(Name = "Име")]
        [Required(ErrorMessage = "Моля попълнете име", AllowEmptyStrings = false)]
        public string contactFirstName { get; set; }

        [Display(Name = "Презиме")]
        public string contactMidleName { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Моля попълнете фамилия", AllowEmptyStrings = false)]
        public string contactLastName { get; set; }

        [Display(Name = "Държава")]
        [Required(ErrorMessage = "Моля попълнете държава", AllowEmptyStrings = false)]
        public string contactCountry { get; set; }

        [Display(Name = "Град")]
        [Required(ErrorMessage = "Моля попълнете град", AllowEmptyStrings = false)]
        public string contactCity { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Моля попълнете адрес", AllowEmptyStrings = false)]
        public string contactAddress { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Моля попълнете телефон-", AllowEmptyStrings = false)]
        public string contactPhone { get; set; }

        [Display(Name = "Имейл")]
        [Required(ErrorMessage = "Моля попълнете имейл", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string contactEmail { get; set; }

        [Display(Name = "Допълнителна информация")]
        public string contactOthers { get; set; }

        [Display(Name = "Ценова група")]
        public SelectList priceGroupList { get; set; }

        [Display(Name = "Станиция")]
        [Required(ErrorMessage = "Станиция е задължителена", AllowEmptyStrings = false)]
        public SelectList stationIdList { get; set; }

        [Authorize]
        public class priceGroupListClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        public class stationIdListClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        public class returnStateWithList
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
            public List<Contact> returnObject;
    }

        public class dataTableReturnClass
        {
            public int totalRecords { get; set; }
            public int filteredRecords { get; set; }
        }

        [Authorize]
        public class Contact
        {
            public int autoId { get; set; }
            public string contactCompanyName { get; set; }
            public string contactFirstName { get; set; }
            public string contactMidleName { get; set; }
            public string contactLastName { get; set; }
            public string contactCountry { get; set; }
            public string contactCity { get; set; }
            public string contactAddress { get; set; }
            public string contactPhone { get; set; }
            public string contactEmail { get; set; }
            public string contactOthers { get; set; }
            public string contactLongitude { get; set; }
            public string contactLatitude { get; set; }
            public int contactCreatedByUserId { get; set; }
            public string priceGroupName { get; set; }
            public int priceGroupId { get; set; }
            public string conctactNearestStationName { get; set; }
            public double contactLongitudeNum { get; set; }
            public double contactLatitudeNum { get; set; }
        }

        [Authorize]
        public List<stationIdListClass> htmlStationIdListClass(string connectionSting){
            List<stationIdListClass> vReturnSelect = new List<stationIdListClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        vReturnSelect.Add(new stationIdListClass
                        {
                            key = "-1",
                            selectLabel = ""
                        });

                        string vSQLString = "select autoId = cast(autoId as nvarchar(50)) + '_{ \"latitude\" : \"' + Cast( IsNull( stationGeoLocation.Lat, 0.0 ) as nvarchar(50) ) + '\",\"longitude\":\"' + cast( IsNull( stationGeoLocation.Long, 0.0 )  as nvarchar(50) ) + '\"}' , stationName from [dbo].[stations] with (nolock) where stationEnabled = 1 and stationDeleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new stationIdListClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["stationName"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        public List<priceGroupListClass> htmlPriceGroupListClass(string connectionSting){
            List<priceGroupListClass> vReturnSelect = new List<priceGroupListClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "select autoId, priceGroupName from [dbo].[priceGroup] with (nolock)";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            vReturnSelect.Add(new priceGroupListClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["priceGroupName"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        [Authorize]
        public dataTableReturnClass getDataTableCounts(string connectionString, string whereClause, List<buildWhereClause> searchArrayObject)
        {
            dataTableReturnClass vResult = new dataTableReturnClass();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLStringTotalCount = "SELECT count(1) as returnData FROM [dbo].[contacts] with (nolock) where contactEnabled = 1 and contactDeleted = 0";
                        string vSQLStringFiltered = "SELECT count(1) as returnData FROM [dbo].[contacts] with (nolock) where contactEnabled = 1 and contactDeleted = 0" + whereClause;
                        var cmd = new SqlCommand(vSQLStringTotalCount, conn);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            vResult.totalRecords = reader.GetInt32(reader.GetOrdinal("returnData"));
                        }
                        cmd = new SqlCommand(vSQLStringFiltered, conn);
                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            vResult.filteredRecords = reader.GetInt32(reader.GetOrdinal("returnData"));
                        }
                    }
                }
                catch (SqlException ex){
                    conn.Close();
                }
                catch (Exception ex){
                    conn.Close();
                }
                finally{
                    conn.Close();
                }
            }


            return vResult;
        }

        [Authorize]
        public List<Contact> getContactList(string userName, string connectionString, string whereClause, List<buildWhereClause> searchArrayObject, string sortBy, string vOffset, string rowLength)
        {
            List<Contact> returnList = new List<Contact>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "SELECT [autoId], [contactCompanyName], [contactFirstName], [contactMidleName], [contactLastName], [contactCountry], [contactCity], [contactAddress], [contactPhone], [contactEmail], [contactOthers], [contactCreatedByUserId], [contactCreationDT], [contactEnabled], [contactDeleted], [priceGroupName], [neareastStationName], [contactLatitudeNum], [contactLongitudeNum]  FROM [dbo].[vwContacts] where contactEnabled = 1 and contactDeleted = 0 " + whereClause + " " + sortBy + "OFFSET " +vOffset+ " ROWS FETCH NEXT " + rowLength + " ROWS ONLY";
                        var cmd = new SqlCommand(vSQLString, conn);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                        };
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addContact = new Contact();
                            addContact.autoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addContact.contactCompanyName = reader["contactCompanyName"].ToString();
                            addContact.contactFirstName = reader["contactFirstName"].ToString();
                            addContact.contactMidleName = reader["contactMidleName"].ToString();
                            addContact.contactLastName = reader["contactLastName"].ToString();
                            addContact.contactCountry = reader["contactCountry"].ToString();
                            addContact.contactCity = reader["contactCity"].ToString();
                            addContact.contactAddress = reader["contactAddress"].ToString();
                            addContact.contactPhone = reader["contactPhone"].ToString();
                            addContact.contactEmail = reader["contactEmail"].ToString();
                            addContact.priceGroupName = reader["priceGroupName"].ToString();
                            addContact.conctactNearestStationName = reader["neareastStationName"].ToString();
                            addContact.contactLatitudeNum = reader.GetDouble(reader.GetOrdinal("contactLatitudeNum"));
                            addContact.contactLongitudeNum = reader.GetDouble(reader.GetOrdinal("contactLongitudeNum"));
                            returnList.Add(addContact);
                            addContact = null;
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }


        [Authorize]
        public returnStateWithList getContactListOrder(string userName, string connectionString, string contactType, string vOrderId)
        {
            returnStateWithList returnObject = new returnStateWithList();
            returnObject.returnObject = new List<Contact>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string joinCondition = (contactType == "sender" ? "orderSenderId" : "orderReceiverId");
                        string vSQLString = "select [contactCompanyName], [contactFirstName], [contactMidleName], [contactLastName], [contactCity], [contactPhone], [contactEmail] from contacts c with (nolock) inner join orders o with (nolock) on o." + joinCondition + " = c.autoId where o.orderId = @orderId";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@orderId", vOrderId);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addContact = new Contact();
                            addContact.contactCompanyName = reader["contactCompanyName"].ToString();
                            addContact.contactFirstName = reader["contactFirstName"].ToString();
                            addContact.contactMidleName = reader["contactMidleName"].ToString();
                            addContact.contactLastName = reader["contactLastName"].ToString();
                            addContact.contactCity = reader["contactCity"].ToString();
                            addContact.contactPhone = reader["contactPhone"].ToString();
                            addContact.contactEmail = reader["contactEmail"].ToString();
                            returnObject.returnObject.Add(addContact);
                            addContact = null;
                        }

                        returnObject.resultState = true;
                        returnObject.resultMessage = "Read succesfully";
                    }
                }
                catch (SqlException ex){
                    returnObject.resultState = false;
                    returnObject.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    returnObject.resultState = false;
                    returnObject.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnObject;
        }

        [Authorize]
        public bool checkFieldExists(string fieldName, string valueToCheck, string connectionSting){
            bool vReturn = false;
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLStringExist = "SELECT count(1) as returnData FROM [dbo].[contacts] where " + fieldName + "=@" + fieldName;
                        var cmd = new SqlCommand(vSQLStringExist, conn);
                        cmd.Parameters.AddWithValue(fieldName, valueToCheck);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturn = ( reader.GetInt32(reader.GetOrdinal("returnData")) >= 1 );
                        }

                    }
                }
                catch (SqlException ex){
                    vReturn = true;
                }
                catch (Exception ex){
                    vReturn = true;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturn;
        }

        [Authorize]
        public returnState checkContactPhoneBoforeSave(Contact newContact, string connectionSting)
        {
            returnState vReturnState = new returnState();
            vReturnState.resultState = true;
            vReturnState.resultMessage = "Phone do not exist";

            if (checkFieldExists("contactPhone", newContact.contactPhone, connectionSting))
            {
                vReturnState.resultState = false;
                vReturnState.resultMessage = "Phone exist";
            }

            return vReturnState;
        }

        public returnState saveNewContact(Contact newContact, string connectionSting)
        {
            returnState vReturnState = new returnState();
            if (checkFieldExists("contactPhone", newContact.contactPhone, connectionSting)){
                vReturnState.resultState = false;
                vReturnState.resultMessage = "Phone exist";
                return vReturnState;
            };

            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[createContacts]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@contactCompanyName", SqlDbType.VarChar).Value = newContact.contactCompanyName;
                        cmd.Parameters.Add("@contactFirstName", SqlDbType.VarChar).Value = newContact.contactFirstName;
                        cmd.Parameters.Add("@contactMidleName", SqlDbType.VarChar).Value = newContact.contactMidleName;
                        cmd.Parameters.Add("@contactLastName", SqlDbType.VarChar).Value = newContact.contactLastName;
                        cmd.Parameters.Add("@contactCountry", SqlDbType.VarChar).Value = newContact.contactCountry;
                        cmd.Parameters.Add("@contactCity", SqlDbType.VarChar).Value = newContact.contactCity;
                        cmd.Parameters.Add("@contactAddress", SqlDbType.VarChar).Value = newContact.contactAddress;
                        cmd.Parameters.Add("@contactPhone", SqlDbType.VarChar).Value = newContact.contactPhone;
                        cmd.Parameters.Add("@contactOthers", SqlDbType.VarChar).Value = newContact.contactOthers;
                        cmd.Parameters.Add("@contactEmail", SqlDbType.VarChar).Value = newContact.contactEmail;
                        cmd.Parameters.Add("@contactCreatedByUserId", SqlDbType.VarChar).Value = newContact.contactCreatedByUserId;
                        cmd.Parameters.Add("@contactLongitude", SqlDbType.VarChar).Value = newContact.contactLongitude;
                        cmd.Parameters.Add("@contactLatitude", SqlDbType.VarChar).Value = newContact.contactLatitude;
                        cmd.Parameters.Add("@priceGroupId", SqlDbType.Int).Value = newContact.priceGroupId;

                        var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState editContact(Contact editContact, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[editContacts]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@contactCompanyName", SqlDbType.VarChar).Value = editContact.contactCompanyName;
                        cmd.Parameters.Add("@contactFirstName", SqlDbType.VarChar).Value = editContact.contactFirstName;
                        cmd.Parameters.Add("@contactMidleName", SqlDbType.VarChar).Value = editContact.contactMidleName;
                        cmd.Parameters.Add("@contactLastName", SqlDbType.VarChar).Value = editContact.contactLastName;
                        cmd.Parameters.Add("@contactCountry", SqlDbType.VarChar).Value = editContact.contactCountry;
                        cmd.Parameters.Add("@contactCity", SqlDbType.VarChar).Value = editContact.contactCity;
                        cmd.Parameters.Add("@contactAddress", SqlDbType.VarChar).Value = editContact.contactAddress;
                        cmd.Parameters.Add("@contactPhone", SqlDbType.VarChar).Value = editContact.contactPhone;
                        cmd.Parameters.Add("@contactOthers", SqlDbType.VarChar).Value = editContact.contactOthers;
                        cmd.Parameters.Add("@contactEmail", SqlDbType.VarChar).Value = editContact.contactEmail;
                        cmd.Parameters.Add("@contactCreatedByUserId", SqlDbType.VarChar).Value = editContact.contactCreatedByUserId;
                        cmd.Parameters.Add("@contactLongitude", SqlDbType.VarChar).Value = editContact.contactLongitude;
                        cmd.Parameters.Add("@contactLatitude", SqlDbType.VarChar).Value = editContact.contactLatitude;
                        cmd.Parameters.Add("@priceGroupId", SqlDbType.Int).Value = editContact.priceGroupId;
                        cmd.Parameters.Add("@current_autoId", SqlDbType.VarChar).Value = editContact.autoId;

                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState deleteContact(Contact editContact, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionSting;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        var cmd = new SqlCommand("[dbo].[deleteContact]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@contactAutoId", SqlDbType.VarChar).Value = editContact.autoId;

                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex){
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally{
                    conn.Close();
                }

            }
            return vReturnState;
        }


    }
}