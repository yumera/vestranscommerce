﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace startProject1.Models
{
    public class StationsViewModel : Controller
    {
        public bool canEdit { get; set; } = false;

        [Display(Name = "Име на станция: ")]
        [Required(ErrorMessage = "Моля попълнете име на станция", AllowEmptyStrings = false)]
        public string stationName { get; set; }

        [Display(Name = "Държава")]
        [Required(ErrorMessage = "Моля попълнете държава", AllowEmptyStrings = false)]
        public string stationCountry { get; set; }

        [Display(Name = "Град")]
        [Required(ErrorMessage = "Моля попълнете град", AllowEmptyStrings = false)]
        public string stationCity { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Моля попълнете адрес", AllowEmptyStrings = false)]
        public string stationAddress { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Моля попълнете телефон", AllowEmptyStrings = false)]
        public string stationPhone { get; set; }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        [Authorize]
        public class selectValueClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        [Authorize]
        public class Stations
        {
            public int stationAutoId { get; set; }
            public string stationName { get; set; }
            public string stationCountry { get; set; }
            public string stationCity { get; set; }
            public string stationAddress { get; set; }
            public string stationPhone { get; set; }
            public string stationLongitude { get; set; }
            public string stationLatitude { get; set; }

        }

        public returnState saveNewStation(Stations newStation, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[createStations]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@stationCountry", SqlDbType.VarChar).Value = newStation.stationCountry;
                        cmd.Parameters.Add("@stationCity", SqlDbType.VarChar).Value = newStation.stationCity;
                        cmd.Parameters.Add("@stationAddress", SqlDbType.VarChar).Value = newStation.stationAddress;
                        cmd.Parameters.Add("@stationName", SqlDbType.VarChar).Value = newStation.stationName;
                        cmd.Parameters.Add("@stationPhone", SqlDbType.VarChar).Value = newStation.stationPhone;
                        cmd.Parameters.Add("@stationLongitude", SqlDbType.VarChar).Value = newStation.stationLongitude;
                        cmd.Parameters.Add("@stationLatitude", SqlDbType.VarChar).Value = newStation.stationLatitude;

                        var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        public returnState deleteStation(string vStationAutoId, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[deleteStations]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@station_autoId", SqlDbType.VarChar).Value = vStationAutoId;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        public returnState editStation(Stations existingStation, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[editStations]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@stationCountry", SqlDbType.VarChar).Value = existingStation.stationCountry;
                        cmd.Parameters.Add("@stationCity", SqlDbType.VarChar).Value = existingStation.stationCity;
                        cmd.Parameters.Add("@stationAddress", SqlDbType.VarChar).Value = existingStation.stationAddress;
                        cmd.Parameters.Add("@stationName", SqlDbType.VarChar).Value = existingStation.stationName;
                        cmd.Parameters.Add("@stationPhone", SqlDbType.VarChar).Value = existingStation.stationPhone;
                        cmd.Parameters.Add("@selectedAutoId", SqlDbType.VarChar).Value = existingStation.stationAutoId;
                        cmd.Parameters.Add("@stationLongitude", SqlDbType.VarChar).Value = existingStation.stationLongitude;
                        cmd.Parameters.Add("@stationLatitude", SqlDbType.VarChar).Value = existingStation.stationLatitude;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }


        public List<Stations> getStationList(string userName, string connectionString)
        {
            List<Stations> returnList = new List<Stations>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "select [autoId], [stationName], [stationCountry], [stationCity], [stationAddress], [stationPhone] from [dbo].[stations] where stationEnabled = 1 and stationDeleted = 0";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addStation = new Stations();
                            addStation.stationAutoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addStation.stationName = reader["stationName"].ToString();
                            addStation.stationCountry = reader["stationCountry"].ToString();
                            addStation.stationCity = reader["stationCity"].ToString();
                            addStation.stationAddress = reader["stationAddress"].ToString();
                            addStation.stationPhone = reader["stationPhone"].ToString();
                            returnList.Add(addStation);
                            addStation = null;
                        }
                    }
                }
                catch (SqlException ex)
                {
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnList;
        }

        public List<selectValueClass> getStationListAsDropDown(string userName, string whereClause, string connectionString)
        {
            List<selectValueClass> vReturnSelect = new List<selectValueClass>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "select [autoId], [stationName] from [dbo].[stations] where stationEnabled = 1 and stationDeleted = 0 and " + whereClause;
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new selectValueClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["stationName"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return vReturnSelect;
        }

    }
}