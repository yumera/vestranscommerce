﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static startProject1.Models.OrderFormModel;

namespace startProject1.Models
{
    public class UsersViewModel : Controller
    {
        [Display(Name = "Потребител")]
        [Required(ErrorMessage = "Моля попълнете име на потребител", AllowEmptyStrings = false)]
        public string loginName { get; set; }

        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "Паролата {0} трябва да бъде минимум {2} символа", MinimumLength = 6)]
        [Display(Name = "Парола")]
        [Required(ErrorMessage = "Моля попълнете парола", AllowEmptyStrings = false)]
        public string loginPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потвърдете паролата")]
        [Required(ErrorMessage = "Моля потвърдете парола", AllowEmptyStrings = false)]
        [System.ComponentModel.DataAnnotations.Compare("loginPassword", ErrorMessage = "Въведените пароли не съвпадат")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Дъжава")]
        [Required(ErrorMessage = "Моля попълнете държава", AllowEmptyStrings = false)]
        public string userCountry { get; set; }

        [Display(Name = "Град")]
        [Required(ErrorMessage = "Моля попълнете град", AllowEmptyStrings = false)]
        public string userCity { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Моля попълнете адрес", AllowEmptyStrings = false)]
        public string userAddress { get; set; }

        [Display(Name = "Пълно име на потребител")]
        [Required(ErrorMessage = "Моля попълнете име", AllowEmptyStrings = false)]
        public string userNames { get; set; }

        [Display(Name = "Телфон")]
        [Required(ErrorMessage = "Моля попълнете телефон", AllowEmptyStrings = false)]
        public string userPhone { get; set; }

        [Display(Name = "Емейл")]
        [Required(ErrorMessage = "Моля попълнете имейл", AllowEmptyStrings = false)]
        public string userEmail { get; set; }

        public int loginAutoId { get; set; }
        public int userRightId { get; set; }

        [Display(Name = "Потребителки права")]
        /*[Required(ErrorMessage = "Моля попълнете права", AllowEmptyStrings = false)]*/
        public SelectList userRightSelectList { get; set; }

        [Display(Name = "Станция по подразбиране")]
        [Required(ErrorMessage = "Моля попълнете права", AllowEmptyStrings = false)]
        public SelectList userDefaultStationId { get; set; }

        public class returnState
        {
            public bool resultState { get; set; }
            public string resultMessage { get; set; }
        }

        [Authorize]
        public class userRightsClass
        {
            public string key { get; set; }
            public string selectLabel { get; set; }
        }

        [Authorize]
        public class Login
        {
            public string loginName { get; set; }
            public string loginPassword { get; set; }
            public string userCountry { get; set; }
            public string userCity { get; set; }
            public string userAddress { get; set; }
            public string userNames { get; set; }
            public string userPhone { get; set; }
            public string userEmail { get; set; }
            public string userRight { get; set; }
            public string stationName { get; set; }
            public int userRightId { get; set; }
            public int loginAutoId { get; set; }
            public int userDefaultStationId { get; set; }
        }

        [Authorize]
        public List<userRightsClass> htmlSelectUserRights(string connectionSting)
        {
            List<userRightsClass> vReturnSelect = new List<userRightsClass>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "select autoId, userRightDescription from [dbo].[userRights]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            vReturnSelect.Add(new userRightsClass
                            {
                                key = reader["autoId"].ToString(),
                                selectLabel = reader["userRightDescription"].ToString()
                            });
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }

            }

            return vReturnSelect;
        }

        [Authorize]
        public returnState changePasswordUser(string loginName, string loginPassword, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[changePassworLogin]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@loginName", SqlDbType.VarChar).Value = loginName;
                        cmd.Parameters.Add("@loginPassword", SqlDbType.VarChar).Value = loginPassword;
                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Паролата сменена успешно";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = "Грешка при смяна на парола : " + ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = "Грешка при смяна на парола : " + ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState deleteUser(int loginAutoId, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[deleteLogin]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@loginAutoId", SqlDbType.Int).Value = loginAutoId;
                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState editUser(Login editLogin, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[editLogin]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@loginAutoId", SqlDbType.VarChar).Value = editLogin.loginAutoId;
                        cmd.Parameters.Add("@loginName", SqlDbType.VarChar).Value = editLogin.loginName;
                        cmd.Parameters.Add("@userCountry", SqlDbType.VarChar).Value = editLogin.userCountry;
                        cmd.Parameters.Add("@userCity", SqlDbType.VarChar).Value = editLogin.userCity;
                        cmd.Parameters.Add("@userAddress", SqlDbType.VarChar).Value = editLogin.userAddress;
                        cmd.Parameters.Add("@userName", SqlDbType.VarChar).Value = editLogin.userNames;
                        cmd.Parameters.Add("@userPhone", SqlDbType.VarChar).Value = editLogin.userPhone;
                        cmd.Parameters.Add("@userEmail", SqlDbType.VarChar).Value = editLogin.userEmail;
                        cmd.Parameters.Add("@userRightId", SqlDbType.VarChar).Value = editLogin.userRightId;
                        cmd.Parameters.Add("@userDefaultStationId", SqlDbType.VarChar).Value = editLogin.userDefaultStationId;

                        cmd.ExecuteNonQuery();

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public returnState saveNewUser(Login newLogin, string connectionSting)
        {
            returnState vReturnState = new returnState();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionSting;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        var cmd = new SqlCommand("[dbo].[createLogin]", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@loginName", SqlDbType.VarChar).Value = newLogin.loginName;
                        cmd.Parameters.Add("@loginPassword", SqlDbType.VarChar).Value = newLogin.loginPassword;
                        cmd.Parameters.Add("@userCountry", SqlDbType.VarChar).Value = newLogin.userCountry;
                        cmd.Parameters.Add("@userCity", SqlDbType.VarChar).Value = newLogin.userCity;
                        cmd.Parameters.Add("@userAddress", SqlDbType.VarChar).Value = newLogin.userAddress;
                        cmd.Parameters.Add("@userName", SqlDbType.VarChar).Value = newLogin.userNames;
                        cmd.Parameters.Add("@userPhone", SqlDbType.VarChar).Value = newLogin.userPhone;
                        cmd.Parameters.Add("@userEmail", SqlDbType.VarChar).Value = newLogin.userEmail;
                        cmd.Parameters.Add("@userRightId", SqlDbType.VarChar).Value = newLogin.userRightId;
                        cmd.Parameters.Add("@userDefaultStationId", SqlDbType.VarChar).Value = newLogin.userDefaultStationId;

                        var returnParameter = cmd.Parameters.Add("@return_autoId", SqlDbType.Int);
                        returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.ExecuteNonQuery();
                        vReturnState.resultState = ((int)returnParameter.Value > 0);

                        vReturnState.resultState = true;
                        vReturnState.resultMessage = "Saved succesfully";
                    }
                }
                catch (SqlException ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                catch (Exception ex)
                {
                    vReturnState.resultState = false;
                    vReturnState.resultMessage = ex.Message;
                }
                finally
                {
                    conn.Close();
                }

            }
            return vReturnState;
        }

        [Authorize]
        public List<Login> getUserList(string userName, string connectionString)
        {
            List<Login> returnList = new List<Login>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                try
                {
                    conn.Open();
                    if (conn.State == ConnectionState.Open)
                    {
                        string vSQLString = "SELECT [autoId],[loginName],[userCountry],[userCity],[userAddress],[userNames],[userPhone],[userEmail],[userRight],[stationName] FROM [dbo].[vwUsers]";
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            var addLogin = new Login();
                            addLogin.loginAutoId = reader.GetInt32(reader.GetOrdinal("autoId"));
                            addLogin.loginName = reader["loginName"].ToString();
                            addLogin.userCountry = reader["userCountry"].ToString();
                            addLogin.userCity = reader["userCity"].ToString();
                            addLogin.userAddress = reader["userAddress"].ToString();
                            addLogin.userNames = reader["userNames"].ToString();
                            addLogin.userPhone = reader["userPhone"].ToString();
                            addLogin.userEmail = reader["userEmail"].ToString();
                            addLogin.userRight = reader["userRight"].ToString();
                            addLogin.stationName = reader["stationName"].ToString();
                            returnList.Add(addLogin);
                            addLogin = null;
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnList;
        }

        [Authorize]
        public int getUserIdByName(string userName, string fieldName, string connectionString)
        {
            int returnUserIdt = -1;
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[loginName], [userDefaultStationId], [userRight] FROM [dbo].[vwUsers] where loginName = @loginName";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@loginName", userName);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            returnUserIdt = reader.GetInt32(reader.GetOrdinal(fieldName));
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnUserIdt;
        }


        [Authorize]
        public static int getUserIdByNameStatic(string userName, string fieldName, string connectionString)
        {
            int returnUserIdt = -1;
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT [autoId],[loginName], [userDefaultStationId], [userRight] FROM [dbo].[vwUsers] where loginName = @loginName";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@loginName", userName);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            returnUserIdt = reader.GetInt32(reader.GetOrdinal(fieldName));
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnUserIdt;
        }

        [Authorize]
        public static string getUserFieldByName(string userName, string fieldName, string connectionString)
        {
            string returnUserData = "";
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT "+ fieldName + " as name FROM [dbo].[vwUsers] where loginName = @userName";
                        var cmd = new SqlCommand(vSQLString, conn);
                        cmd.Parameters.AddWithValue("@userName", System.Net.WebUtility.UrlDecode(userName.ToString()));
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            returnUserData = reader["name"].ToString(); ;
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnUserData;
        }

        private string getQueryFromCommand(SqlCommand cmd)
        {
            string CommandTxt = cmd.CommandText;


            foreach (SqlParameter parms in cmd.Parameters)
            {
                string val = String.Empty;
                if (parms.DbType.Equals(DbType.String) || parms.DbType.Equals(DbType.DateTime))
                    val = "'" + Convert.ToString(parms.Value).Replace(@"\", @"\\").Replace("'", @"\'") + "'";
                if (parms.DbType.Equals(DbType.Int16) || parms.DbType.Equals(DbType.Int32) || parms.DbType.Equals(DbType.Int64) || parms.DbType.Equals(DbType.Decimal) || parms.DbType.Equals(DbType.Double))
                    val = Convert.ToString(parms.Value);
                string paramname = parms.ParameterName;
                CommandTxt = CommandTxt.Replace("@" + paramname, val);
            }
            return (CommandTxt);
        }

        [Authorize]
        public string getFieldByTableAndCondition(string tableName, string fieldName, string whereCondition, List<buildWhereClause> searchArrayObject, bool equalNotLike, string connectionString)
        {
            string returnUserData = "";
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT name = " + fieldName + " FROM " + tableName + " with (nolock) where " + whereCondition;
                        var cmd = new SqlCommand(vSQLString, conn);
                        for (int i = 0; i < searchArrayObject.Count; i++)
                        {
                            if (equalNotLike) { 
                                cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, searchArrayObject[i].paramValue);
                            } else
                            {
                                cmd.Parameters.AddWithValue(searchArrayObject[i].paramName, "%" + searchArrayObject[i].paramValue + "%");
                            }
                        };
                        //getQueryFromCommand(cmd);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            returnUserData = reader["name"].ToString();
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnUserData;
        }

        [Authorize]
        public string getFieldByTableAndConditionSafe(string tableName, string fieldName, string whereCondition, string connectionString)
        {
            string returnUserData = "";
            using (SqlConnection conn = new SqlConnection()){
                conn.ConnectionString = connectionString;
                try{
                    conn.Open();
                    if (conn.State == ConnectionState.Open){
                        string vSQLString = "SELECT name = " + fieldName + " FROM " + tableName + " with (nolock) where " + whereCondition;
                        var cmd = new SqlCommand(vSQLString, conn);
                        var reader = cmd.ExecuteReader();
                        while (reader.Read()){
                            returnUserData = reader["name"].ToString();
                        }
                    }
                }
                catch (SqlException ex){
                }
                catch (Exception ex){
                }
                finally{
                    conn.Close();
                }
            }
            return returnUserData;
        }


    }
}