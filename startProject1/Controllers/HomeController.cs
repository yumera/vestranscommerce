﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using startProject1.Models;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static startProject1.Models.LoginController;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel;
using System.Net;
using System.Threading;
using System.Security.Principal;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using static startProject1.Models.OrderFormModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using static startProject1.Models.SetDetailsController;
using static startProject1.Models.UsersViewModel;
using static startProject1.Models.ContactsAddress;


namespace startProject1.Controllers
{
    public class HomeController : Controller
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
            services.AddMvc();
        }

        private readonly AppSettings _АppSettings;

        public HomeController(IOptions<AppSettings> appSettings)
        {
            //var appConfig2 = configuration.GetSection("ConnectionStrings");
            _АppSettings = appSettings.Value;
        }

        public IActionResult Index()
        {
            var model = new OrderFormModel();
            model.courierIdList = new SelectList(model.htmlCourierIdListClass(_АppSettings.vestrancecommersString), "key", "selectLabel");
            ViewData["Title"] = "Rexcourier";
            if (User.Identity.IsAuthenticated) {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Title"] = "Rexcourier";
            ViewData["Message"] = "София, улица Странджа 89, ап.2";
            ViewData["Message2"] = "тел: +359 2 832 00 50, мобилен: +359 889 266 555";
            return View();
        }

        public IActionResult Contact()
        {
            var model = new UsersViewModel();
            ViewData["Title"] = "Rexcourier";
            ViewData["Message"] = "София, улица Странджа 89, ап.2";
            ViewData["Message2"] = "тел: +359 2 832 00 50, мобилен: +359 889 266 555";
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }
            return View(model);
        }

        public IActionResult Login()
        {
            return View();
        }

        [Authorize]
        public IActionResult AdminForm()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }
            return View();
        }

        /*public IActionResult Logout()
        {
            return View();
        }*/

        [Authorize]
        public async Task<IActionResult> Logout(string userName)
        {
            try
            {
                /*IPrincipal currentPrincipal = Thread.CurrentPrincipal;
                var user = User as ClaimsPrincipal;
                var identity = user.Identity as ClaimsIdentity;
                var claim = (from c in user.Claims where c.Value == userName select c).Single();
                if (claim != null){
                    //identity.RemoveClaim(claim);
                    identity.TryRemoveClaim(claim);
                    identity.AddClaim(new Claim("AnnounceCount", "Updated Value"));
                }*/
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch
            {
                ModelState.AddModelError("CHECK_LOGOUT", "Грешка при изход");
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(Models.LoginController user)
        {
            if (ModelState.IsValid)
            {
                string strConnectionString = _АppSettings.vestrancecommersLoginString;
                strConnectionString = strConnectionString.Replace("@dbName", _АppSettings.DBNAME);
                connectionReturnState functionReturn = user.checkUser(System.Net.WebUtility.UrlDecode(user.userName.ToString()), user.password, strConnectionString);
                if (functionReturn.connectionBoolState)
                {
                    List<Claim> claims = new List<Claim>{
                        new Claim( ClaimTypes.Name, System.Net.WebUtility.UrlDecode(user.userName.ToString()) ),
                        new Claim( ClaimTypes.Email, functionReturn.userEmail ),
                        new Claim( ClaimTypes.Role, functionReturn.userRights ),
                        new Claim( ClaimTypes.UserData, functionReturn.userStation )
                    };

                    var userIdentity = new ClaimsIdentity(claims, "loggedUser");

                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                    Thread.CurrentPrincipal = principal;
                    await HttpContext.SignInAsync(principal);
                    //var token = FederatedAuthentication.SessionAuthenticationModule.CreateSessionSecurityToken(principal, "Api Test", DateTime.UtcNow, DateTime.UtcNow.AddMinutes(30), true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("CHECK_LOGIN", functionReturn.connectionStringState.ToString());
                }
            }
            return View(user);
        }

        /*#####################################  START ORDER FUNTIONS ########################################### */
        [Authorize]
        public IActionResult OrderForm()
        {
            var model = new OrderFormModel();
            string vSqlToExecute = "select rownum as autoId, contactCity as name from [dbo].[vwSenderReceiverOrderInfo]";
            model.ordersFromCity = new SelectList(model.fillSelectForOrders(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "selectLabel");
            vSqlToExecute = "select rownum as autoId, contactCity as name from [dbo].[vwSenderReceiverOrderInfo]";
            model.ordersToCity = new SelectList(model.fillSelectForOrders(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "selectLabel");
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
                ViewData["hereApiKey"] = _АppSettings.hereApikey;
            }
            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readMainViewOrders()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string userRight = "";
            int userDefaultStationId = 0;
            string whereClause = "where orderCreatedDateTime between DATEADD(month, DATEDIFF(month, 0, {{dateToReplace}}), 0) and DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,{{dateToReplace}})+1,0))";
            string whereClauseTotal = "where orderCreatedDateTime between DATEADD(month, DATEDIFF(month, 0, {{dateToReplace}}), 0) and DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,{{dateToReplace}})+1,0))";
            string sortBy = "";
            int vDraw = 1;
            int pageIndex = 0;
            string pageLength = "";
            int rowLength = 10;
            string searchDate;
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                userRight = UsersViewModel.getUserFieldByName(loggedUser, "[userRightCode]", _АppSettings.vestrancecommersString);
                whereClause = whereClause.Replace("{{dateToReplace}}", "@searchDate");
                searchDate = requestBody.orderCreationDateSearch.ToString();
                if (requestBody.contactCity.ToString() != ""){
                    whereClause += " and contactCity = '" + requestBody.contactCity.ToString() + "'";
                }
                if (requestBody.receiverCity.ToString() != ""){
                    whereClause += " and receiverCity = '" + requestBody.receiverCity.ToString() + "'";
                }
                if (userRight == "StationAdministrator" || userRight == "Courier"){
                    userDefaultStationId = UsersViewModel.getUserIdByNameStatic(loggedUser, "userDefaultStationId", _АppSettings.vestrancecommersString);
                    //whereClause += " and stationID = " + userDefaultStationId.ToString();
                    searchArrayObject.Add(new buildWhereClause() { paramName = "stationID", paramValue = userDefaultStationId.ToString() });
                }
                if (userRight == "Client"){
                    whereClause += " and CreatedBy = '" + loggedUser + "' ";
                }

                whereClauseTotal = whereClause;
                //whereClause += (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.whereClauseSearch).Value.ToString()) ? " and 1=@alwaysOne " : requestBody.whereClauseSearch);
                sortBy = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.sortBy).Value.ToString()) ? " order by orderDeliveryDate desc , orderDeliveryTime desc,autoId desc " : requestBody.sortBy);
                //Get search construction
                if (Convert.ToInt32(requestBody.searchArrayLength.ToString()) > 0)
                {
                    whereClause += " and ( ";
                    for (int i = 0; i < Convert.ToInt32(requestBody.searchArrayLength.ToString()); i++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArray[i].paramName;
                        paramToAdd.paramValue = requestBody.searchArray[i].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (i < Convert.ToInt32(requestBody.searchArrayLength.ToString()) - 1 ? " " + requestBody.searchArray[i].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                //Get global search construction
                if (Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) > 0 ) {
                    whereClause += " and ( ";
                    for (int j = 0; j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()); j++) {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArrayGlobal[j].paramName;
                        paramToAdd.paramValue = requestBody.searchArrayGlobal[j].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + ( j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString())-1 ? " " + requestBody.searchArrayGlobal[j].paramCondition + " " : "" );
                    }
                    whereClause += ")";
                }
                vDraw = requestBody.draw;
                pageIndex = requestBody.currentPage;
                pageLength = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.pageLengthData).Value.ToString()) ? " 10 " : requestBody.pageLengthData);
                rowLength = int.Parse( String.IsNullOrEmpty(requestBody.rowLength.ToString()) ? "10" : requestBody.rowLength.ToString() );
            };

            var model = new OrderFormModel();
            var result = model.getOrderMainViewList(loggedUser, whereClause, searchDate, searchArrayObject, sortBy, (pageIndex * rowLength).ToString(), pageLength, _АppSettings.vestrancecommersString);
            var resultDataTable = model.getDataTableCounts(_АppSettings.vestrancecommersString, whereClauseTotal, whereClause, searchDate, searchArrayObject);
            return Json(new
            {
                draw = vDraw,
                recordsTotal = resultDataTable.totalRecords,
                recordsFiltered = resultDataTable.filteredRecords,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult assignCourier()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            string courierId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                orderId = System.Net.WebUtility.UrlDecode(requestBody.orderId.ToString());
                courierId = System.Net.WebUtility.UrlDecode(requestBody.courierId.ToString());
            };

            var model = new OrderFormModel();
            var result = model.assignCourier(loggedUser, orderId, courierId, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readMainViewOrdersNearestStation()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string userRight = "";
            string userStationId = "";
            int userDefaultStationId = 0;
            string whereClause = "";
            string whereClauseTotal = "";
            string sortBy = "";
            int vDraw = 1;
            int pageIndex = 0;
            string pageLength = "";
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                userStationId = UsersViewModel.getUserFieldByName(loggedUser, "[userDefaultStationId]", _АppSettings.vestrancecommersString);
                if (requestBody.readMainViewCollectableOrders == true) {
                    whereClause = "where statusOrderFinished = 0 and statusOrderCollectedFromCourier = 0 and userRight = 'Клиент' and senderNearestStationId = " + userStationId;
                    whereClauseTotal = "where statusOrderFinished = 0 and userRight = 'Клиент' and senderNearestStationId = " + userStationId;
                }
                else {
                    whereClause = "where statusOrderFinished = 0 and nearestDeliveryStationId = " + userStationId;
                    whereClauseTotal = "where statusOrderFinished = 0 and nearestDeliveryStationId = " + userStationId;
                }
                userRight = UsersViewModel.getUserFieldByName(loggedUser, "[userRightCode]", _АppSettings.vestrancecommersString);
                if (userRight == "StationAdministrator" || userRight == "Courier")
                {
                    userDefaultStationId = UsersViewModel.getUserIdByNameStatic(loggedUser, "userDefaultStationId", _АppSettings.vestrancecommersString);
                    //whereClause += " and stationID = " + userDefaultStationId.ToString();
                    searchArrayObject.Add(new buildWhereClause() { paramName = "stationID", paramValue = userDefaultStationId.ToString() });
                    if (userRight == "Courier") {
                        int userPrimaryKey = 0;
                        userPrimaryKey = UsersViewModel.getUserIdByNameStatic(loggedUser, "autoId", _АppSettings.vestrancecommersString);
                        searchArrayObject.Add(new buildWhereClause() { paramName = "courierAutoId", paramValue = userPrimaryKey.ToString() });
                        whereClause += " and courierAutoId = " + userPrimaryKey.ToString();
                    }
                }
                whereClauseTotal = whereClause;
                sortBy = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.sortBy).Value.ToString()) ? " order by autoId desc " : requestBody.sortBy);
                //Get search construction
                if (Convert.ToInt32(requestBody.searchArrayLength.ToString()) > 0)
                {
                    whereClause += " and ( ";
                    for (int i = 0; i < Convert.ToInt32(requestBody.searchArrayLength.ToString()); i++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArray[i].paramName;
                        paramToAdd.paramValue = requestBody.searchArray[i].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (i < Convert.ToInt32(requestBody.searchArrayLength.ToString()) - 1 ? " " + requestBody.searchArray[i].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                //Get global search construction
                if (Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) > 0)
                {
                    whereClause += " and ( ";
                    for (int j = 0; j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()); j++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArrayGlobal[j].paramName;
                        paramToAdd.paramValue = requestBody.searchArrayGlobal[j].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) - 1 ? " " + requestBody.searchArrayGlobal[j].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                vDraw = requestBody.draw;
                pageIndex = requestBody.currentPage;
                pageLength = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.pageLengthData).Value.ToString()) ? " 10 " : requestBody.pageLengthData);
            };

            var model = new OrderFormModel();
            var result = model.getOrderMainViewListNearestStation(loggedUser, whereClause, searchArrayObject, sortBy, (pageIndex * int.Parse(pageLength)).ToString(), pageLength, _АppSettings.vestrancecommersString);
            var resultDataTable = model.getDataTableCounts(_АppSettings.vestrancecommersString, whereClauseTotal, whereClause, "", searchArrayObject);
            return Json(new
            {
                draw = vDraw,
                recordsTotal = resultDataTable.totalRecords,
                recordsFiltered = resultDataTable.filteredRecords,
                data = result.returnObject
            });
        }


        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult getPricessList()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string returnType = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                returnType = requestBody.returnType;
            };

            var model = new OrderFormModel();
            var result = new returnStatePriceLists();
            switch (returnType) {
                case "orderInterCityPriceId":
                    result = model.getInterCityPricesList(loggedUser, _АppSettings.vestrancecommersString);
                    break;
                case "orderExtraPricesId":
                    result = model.getExtraServicesList(loggedUser, _АppSettings.vestrancecommersString);
                    break;
                case "orderCityPriceId":
                    result = model.getCityPricessList(loggedUser, _АppSettings.vestrancecommersString);
                    break;
            }
            return new JsonResult(result);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult getOrderFullViewOrders()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            string whereClause = "where orderId = @orderId";
            using (StreamReader reader = new StreamReader(stream)) {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                orderId = requestBody.orderId;
                //whereClause += orderId + "'";
            };

            var model = new OrderFormModel();
            var result = model.getOrderFullViewList(loggedUser, whereClause, orderId, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }


        /*#####################################  END ORDER FUNTIONS ########################################### */

        /*#####################################  START ORDER DETAILS FUNTIONS ########################################### */
        [Authorize]
        public IActionResult OrderDetail(string orderStatus, string orderId)
        {
            var model = new OrderFormModel();
            string loggedUser = User.Claims.Where(c => c.Type == System.Security.Claims.ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            string vSqlToExecute = "select autoId as autoId, orderTypeName as name from [dbo].[orderTypes] where orderTypeEnabled = 1";
            model.orderTypesList = new SelectList(model.fillSelectForOrders(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "selectLabel");
            vSqlToExecute = "select autoId as autoId, chashOnDeliveryName as name from [dbo].[cashOnDeliveryTypes] where enabled = 1";
            model.cashOnDeliveryTypeList = new SelectList(model.fillSelectForOrders(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "selectLabel");
            model.stationIdList = new SelectList(model.htmlStationIdListClass(_АppSettings.vestrancecommersString), "key", "selectLabel");
            var modelContact = new ContactsAddress();
            model.priceGroupList = new SelectList(modelContact.htmlPriceGroupListClass(_АppSettings.vestrancecommersString), "key", "selectLabel");

            if (!string.IsNullOrEmpty(orderStatus)) {
                ViewData["OrderTypeCheck"] = orderStatus;
                if (orderStatus == "newOrder") {
                    ViewData["OrderTypeString"] = "Нова поръчка";
                }
                if (orderStatus == "edit" && (!string.IsNullOrEmpty(orderId))) {
                    ViewData["OrderTypeString"] = "Редактиране на поръчка : " + orderId.ToString();
                    ViewData["orderId"] = orderId;
                    model.lockOrder(orderId, loggedUser, _АppSettings.vestrancecommersString);
                }
            }
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
                ViewData["hereApiKey"] = _АppSettings.hereApikey;
            }
            return View(model);
        }



        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult checkOrderIsLocked()
        {
            dynamic requestBody;
            dynamic returnJSON =  new JObject();
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
                orderId = requestBody.dataJSON.orderId;
            };

            var model = new UsersViewModel();
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            searchArrayObject.Clear();
            searchArrayObject.Add(new buildWhereClause() { paramName = "orderId", paramValue = orderId });
            string orderIsLocked = model.getFieldByTableAndCondition("[dbo].[orders]", "cast( IsNull(orderLocked, 0 ) as nvarchar(1) ) + ':' + IsNull(orderLockedByUserName, '"+ loggedUser + "' )", "orderId = @orderId", searchArrayObject, true, _АppSettings.vestrancecommersString);
            bool isOrderIsLocked = ( "1" == orderIsLocked.Substring(0, orderIsLocked.IndexOf(":") ) ? true : false );
            string orderIsLockedBy = orderIsLocked.Substring(orderIsLocked.IndexOf(":") +1);
            if (isOrderIsLocked) {
                if (orderIsLockedBy != loggedUser) {
                    returnJSON.orderIsLocked = true;
                    returnJSON.orderIsLockedBy = orderIsLockedBy;
                } else {
                    returnJSON.orderIsLocked = false;
                }
            }
            else {
                returnJSON.orderIsLocked = false;
            }

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult unlockOrder(){
            dynamic requestBody;
            dynamic returnJSON =  new JObject();
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
                orderId = requestBody.dataJSON.orderId;
            };

            var model = new OrderFormModel();
            returnJSON.orderIsLocked = ( model.unLockOrder(orderId, loggedUser, _АppSettings.vestrancecommersString) == 0 ? false : true);
            return new JsonResult(returnJSON);
        }


        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult orderDetailsContactSearchTable()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string userRight = "";
            string whereClause = "";
            string sortBy = "";
            int rowLength = 10;
            int vDraw = 1;
            int pageIndex = 0;
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            using (StreamReader reader = new StreamReader(stream)){
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                userRight = UsersViewModel.getUserFieldByName(loggedUser, "[userRightCode]", _АppSettings.vestrancecommersString);
                //whereClause = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.whereClause).Value.ToString()) ? " and 1=1 " : requestBody.whereClause);
                //If the user is client he should only see own created contacts
                if (userRight == "Client"){
                    whereClause += " and loginName = '" + loggedUser + "'";
                }
                //Get search construction
                if (Convert.ToInt32(requestBody.searchArrayLength.ToString()) > 0){
                    whereClause += " and ( ";
                    for (int i = 0; i < Convert.ToInt32(requestBody.searchArrayLength.ToString()); i++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArray[i].paramName;
                        paramToAdd.paramValue = requestBody.searchArray[i].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (i < Convert.ToInt32(requestBody.searchArrayLength.ToString()) - 1 ? " " + requestBody.searchArray[i].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                //Get global search construction
                if (Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) > 0){
                    whereClause += " and ( ";
                    for (int j = 0; j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()); j++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArrayGlobal[j].paramName;
                        paramToAdd.paramValue = requestBody.searchArrayGlobal[j].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) - 1 ? " " + requestBody.searchArrayGlobal[j].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                sortBy = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.sortBy).Value.ToString()) ? " order by autoId " : requestBody.sortBy);
                rowLength = int.Parse(((Newtonsoft.Json.Linq.JValue)requestBody.rowLength).Value.ToString());
                vDraw = requestBody.draw;
                pageIndex = requestBody.currentPage;

            };

            var model = new OrderFormModel();
            var result = model.getContactList(loggedUser, _АppSettings.vestrancecommersString, whereClause, searchArrayObject, sortBy, (pageIndex * rowLength).ToString(), rowLength.ToString());
            var modelAddress = new ContactsAddress();
            var resultDataTable = modelAddress.getDataTableCounts(_АppSettings.vestrancecommersString, whereClause, searchArrayObject);
            return Json(new
            {
                draw = vDraw,
                recordsTotal = resultDataTable.totalRecords,
                recordsFiltered = resultDataTable.filteredRecords,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult orderDetailsReceiverSearchTable()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string userRight = "";
            string whereClause = "";
            string sortBy = "";
            int vDraw = 1;
            int pageIndex = 0;
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                userRight = UsersViewModel.getUserFieldByName(loggedUser, "[userRightCode]", _АppSettings.vestrancecommersString);
                //whereClause = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.whereClause).Value.ToString()) ? " and 1=1 " : requestBody.whereClause);
                //If the user is client he should only see own created contacts
                if (userRight == "Client"){
                    whereClause += " and loginName = '" + loggedUser + "'";
                }
                //Get search construction
                if (Convert.ToInt32(requestBody.searchArrayLength.ToString()) > 0)
                {
                    whereClause += " and ( ";
                    for (int i = 0; i < Convert.ToInt32(requestBody.searchArrayLength.ToString()); i++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArray[i].paramName;
                        paramToAdd.paramValue = requestBody.searchArray[i].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (i < Convert.ToInt32(requestBody.searchArrayLength.ToString()) - 1 ? " " + requestBody.searchArray[i].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                //Get global search construction
                if (Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) > 0)
                {
                    whereClause += " and ( ";
                    for (int j = 0; j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()); j++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArrayGlobal[j].paramName;
                        paramToAdd.paramValue = requestBody.searchArrayGlobal[j].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) - 1 ? " " + requestBody.searchArrayGlobal[j].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                sortBy = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.sortBy).Value.ToString()) ? " order by autoId " : requestBody.sortBy);
                vDraw = requestBody.draw;
                pageIndex = requestBody.currentPage;
            };

            var model = new OrderFormModel();
            var result = model.getReceiverList(loggedUser, _АppSettings.vestrancecommersString, whereClause, searchArrayObject, sortBy, (pageIndex * 10).ToString());
            var modelAddress = new ContactsAddress();
            var resultDataTable = modelAddress.getDataTableCounts(_АppSettings.vestrancecommersString, whereClause, searchArrayObject);
            return Json(new
            {
                draw = vDraw,
                recordsTotal = resultDataTable.totalRecords,
                recordsFiltered = resultDataTable.filteredRecords,
                data = result.returnObjectReceiver
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult orderDetailsSaveNewOrder()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            newOrderClass newOrderVar = new newOrderClass();
            var userModel = new UsersViewModel();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                newOrderVar.orderCreatedByUserID = userModel.getUserIdByName(loggedUser, "autoId", _АppSettings.vestrancecommersString);
                newOrderVar.stationID = userModel.getUserIdByName(loggedUser, "userDefaultStationId", _АppSettings.vestrancecommersString);
                newOrderVar.orderSenderId = requestBody.orderSenderId;
                newOrderVar.orderReceiverId = requestBody.orderReceiverId;
                newOrderVar.orderTypeId = requestBody.orderTypeId;
                newOrderVar.orderIsDocument = requestBody.orderIsDocument;
                newOrderVar.orderCount = requestBody.orderCount;
                newOrderVar.orderDescription = requestBody.orderDescription;
                newOrderVar.orderContent = requestBody.orderContent;
                newOrderVar.orderWeight = requestBody.orderWeight;
                newOrderVar.orderWidth = requestBody.orderWidth;
                newOrderVar.orderHeight = requestBody.orderHeight;
                newOrderVar.orderDepth = requestBody.orderDepth;
                newOrderVar.chashOnDelivery = requestBody.chashOnDelivery;
                newOrderVar.chashOnDeliverySum = requestBody.chashOnDeliverySum;
                newOrderVar.orderInsuranceSum = requestBody.orderInsuranceSum;
                newOrderVar.cashOnDeliveryTypeId = requestBody.cashOnDeliveryTypeId;
                newOrderVar.orderDeliveryDate = DateTime.ParseExact(requestBody.orderDeliveryDate.ToString(), "dd.MM.yyyy", null);
                newOrderVar.orderDeliveryTime = requestBody.orderDeliveryTime;
                newOrderVar.orderCityPriceId = requestBody.orderCityPriceId;
                newOrderVar.orderInterCityPriceId = requestBody.orderInterCityPriceId;
                newOrderVar.orderInterCityPriceIdExpress = requestBody.orderInterCityPriceIdExpress;
                newOrderVar.orderExtraPricesId = requestBody.orderExtraPricesId.ToString();
                newOrderVar.orderPriceVat = requestBody.orderPriceVat;
                newOrderVar.orderPriceNoVat = requestBody.orderPriceNoVat;
                newOrderVar.orderImportFileName = requestBody.orderImportFileName;
            };

            var model = new OrderFormModel();
            var result = model.saveOrder(newOrderVar, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult orderDetailsEditOrder()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            newOrderClass editOrderVar = new newOrderClass();
            var userModel = new UsersViewModel();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                editOrderVar.orderId = requestBody.orderId;
                editOrderVar.stationID = userModel.getUserIdByName(loggedUser, "userDefaultStationId", _АppSettings.vestrancecommersString);
                editOrderVar.orderSenderId = requestBody.orderSenderId;
                editOrderVar.orderReceiverId = requestBody.orderReceiverId;
                editOrderVar.orderTypeId = requestBody.orderTypeId;
                editOrderVar.orderIsDocument = requestBody.orderIsDocument;
                editOrderVar.orderCount = requestBody.orderCount;
                editOrderVar.orderDescription = requestBody.orderDescription;
                editOrderVar.orderContent = requestBody.orderContent;
                editOrderVar.orderWeight = requestBody.orderWeight;
                editOrderVar.orderWidth = requestBody.orderWidth;
                editOrderVar.orderHeight = requestBody.orderHeight;
                editOrderVar.orderDepth = requestBody.orderDepth;
                editOrderVar.chashOnDelivery = requestBody.chashOnDelivery;
                editOrderVar.chashOnDeliverySum = requestBody.chashOnDeliverySum;
                editOrderVar.orderInsuranceSum = requestBody.orderInsuranceSum;
                editOrderVar.cashOnDeliveryTypeId = requestBody.cashOnDeliveryTypeId;
                editOrderVar.orderDeliveryDate = DateTime.ParseExact(requestBody.orderDeliveryDate.ToString(), "dd.MM.yyyy", null);
                editOrderVar.orderDeliveryTime = requestBody.orderDeliveryTime;
                editOrderVar.orderCityPriceId = requestBody.orderCityPriceId;
                editOrderVar.orderInterCityPriceId = requestBody.orderInterCityPriceId;
                editOrderVar.orderInterCityPriceIdExpress = requestBody.orderInterCityPriceIdExpress;
                editOrderVar.orderExtraPricesId = requestBody.orderExtraPricesId;
                editOrderVar.orderPriceVat = requestBody.orderPriceVat;
                editOrderVar.orderPriceNoVat = requestBody.orderPriceNoVat;
            };

            var model = new OrderFormModel();
            var result = model.editOrder(editOrderVar, _АppSettings.vestrancecommersString);
            model.unLockOrder(editOrderVar.orderId, loggedUser, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult orderDetailsDeleteOrder()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            newOrderClass editOrderVar = new newOrderClass();
            var userModel = new UsersViewModel();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                editOrderVar.orderId = requestBody.orderId;
            };

            var model = new OrderFormModel();
            var result = model.deleteOrder(editOrderVar, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }

        /*#####################################  END ORDER DETAILS FUNTIONS ########################################### */

        /*#####################################  START SET ORDER DETAILS FUNCTIONS ########################################### */
        [Authorize]
        public IActionResult SetDetails(string orderId)
        {
            var model = new SetDetailsController();
            ViewData["orderId"] = orderId;
            string vSqlToExecute = "";
            vSqlToExecute = "select [autoId], [stationName] as name, 0 as dataSelect from [dbo].[stations] where stationEnabled = 1 and stationDeleted = 0";
            model.stationId = new SelectList(model.fillSelectForOrderDetails(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "value");
            vSqlToExecute = "select [autoId], [statusType] as name, cast( statusOrderFinished as int ) as dataSelect from [dbo].[vwStatuses]";
            model.statusId = new SelectList(model.fillSelectForOrderDetails(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "value");
            var userModel = new UsersViewModel();
            string loggedUser = User.Claims.Where(c => c.Type == System.Security.Claims.ClaimTypes.Name).Select(c => c.Value).SingleOrDefault();
            ViewData["userDefaultStationId"] = userModel.getUserIdByName(loggedUser, "userDefaultStationId", _АppSettings.vestrancecommersString);
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }

            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readOrderDetails()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                orderId = requestBody.orderId;
            };

            var model = new SetDetailsController();
            var result = model.getOrderDetails(loggedUser, orderId, _АppSettings.vestrancecommersString);
            return Json(new
            {
                draw = 1,
                recordsTotal = result.totalCount,
                recordsFiltered = result.totalCount,
                data = result.returnObject
            });
        }

        [AllowAnonymous, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readOrderDetailsAnon()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string orderId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                orderId = requestBody.orderId;
            };

            var model = new SetDetailsController();
            var result = model.getOrderDetailsAnon(loggedUser, orderId, _АppSettings.vestrancecommersString);
            return Json(new
            {
                draw = 1,
                recordsTotal = result.totalCount,
                recordsFiltered = result.totalCount,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveOrderDetail()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            newOrderDetails newOrderDetailsToAdd = new newOrderDetails();
            var userModel = new UsersViewModel();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.dataJSON.userName.ToString());
                newOrderDetailsToAdd.orderId = requestBody.dataJSON.orderId;
                newOrderDetailsToAdd.stationId = requestBody.dataJSON.stationId;
                newOrderDetailsToAdd.userId = userModel.getUserIdByName(loggedUser, "autoId", _АppSettings.vestrancecommersString);
                newOrderDetailsToAdd.orderDetailDescription = requestBody.dataJSON.orderDetailDescription;
                newOrderDetailsToAdd.statusId = requestBody.dataJSON.statusId;
                newOrderDetailsToAdd.canvasAsObject = requestBody.dataJSON.canvasAsObject;
                newOrderDetailsToAdd.canvasAsObjectCollected = requestBody.dataJSON.canvasAsObjectCollected;
            };

            var model = new SetDetailsController();
            var result = model.createOrderDetails(newOrderDetailsToAdd, _АppSettings.vestrancecommersString);
            return new JsonResult(result);
        }
        /*#####################################  END SET ORDER DETAILS FUNCTIONS ########################################### */

        /*#####################################  START GET ORDER DETAILS FUNCTIONS ########################################### */
        [AllowAnonymous]
        public IActionResult GetDetails(string orderId)
        {
            var model = new SetDetailsController();
            ViewData["orderId"] = orderId;
            return View(model);
        }

        /*#####################################  START GET ORDER DETAILS FUNCTIONS ########################################### */

        /*#####################################  START GET CONTACT FUNCTIONS ########################################### */
        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult getContactInformation()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string vContactType = "";
            string vOrderId = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
                vContactType = requestBody.vContactType;
                vOrderId = requestBody.vOrderId;
            }

            var model = new ContactsAddress();
            var result = model.getContactListOrder(loggedUser, _АppSettings.vestrancecommersString, vContactType, vOrderId);
            return new JsonResult(result);
        }
        /*#####################################  END GET CONTACT FUNCTIONS ########################################### */

        /*#####################################  START TESTING FUNCTIONS ########################################### */
        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult testPDFGeneration()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
            };

            var result = requestBody;

            return new JsonResult(result);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult testPDFGeneration2()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = System.Net.WebUtility.UrlDecode(requestBody.userName.ToString());
            };

            var result = requestBody;

            return new JsonResult(result);
        }



        /*#####################################  END TESTING FUNCTIONS ########################################### */
    }
}