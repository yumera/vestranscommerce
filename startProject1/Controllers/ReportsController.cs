﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using startProject1.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace startProject1.Controllers
{
    public class ReportsController : Controller
    {
        private readonly AppSettings _АppSettings;

        public ReportsController(IOptions<AppSettings> appSettings)
        {
            _АppSettings = appSettings.Value;
        }

        [Authorize]
        public IActionResult Reports()
        {
            var model = new ReportsModel();
            string vSqlToExecute = "select autoId as autoId, reportName as name from [dbo].[reports] where reportEnabled = 1 and reportDeleted = 0";
            model.reportNameList = new SelectList(model.fillSelectForReports(_АppSettings.vestrancecommersString, vSqlToExecute), "key", "reportName");
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }
            return View(model);
        }


        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult executeReport(){
            dynamic requestBody;
            dynamic reportParameters;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            int reportId = 0;
            using (StreamReader reader = new StreamReader(stream)){
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
                reportId   = requestBody.reportId;
                reportParameters = requestBody.reportParameters;
            };

            var model = new ReportsModel();
            var resAsJSON = model.callReportByName(loggedUser, reportId, _АppSettings.vestrancecommersString, reportParameters);
            return Json(new
            {
                resultParameters = resAsJSON.resultParameters,
                resultState = resAsJSON.resultState,
                resultMessage = resAsJSON.resultMessage
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult getReportParameters(){
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            int reportId = 0;
            using (StreamReader reader = new StreamReader(stream)){
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
                reportId = requestBody.reportId;
            };

            var model = new ReportsModel();
            var result = "";
            result = model.getReportDataById(loggedUser, reportId, _АppSettings.vestrancecommersString, "reportParameters").resultParameters;
            //var resAsJSON = JsonConvert.DeserializeObject(result);
            dynamic tempParamObj = JsonConvert.DeserializeObject(result);
            //JObject tempParamObj = JObject.Parse(result);
            var resAsJSON = tempParamObj;
            //Check if there is any list which we need to fill out
            int vLoopCount = 0;
            foreach (var parametersObj in tempParamObj){
                if (parametersObj.isList != null && parametersObj.isList.Value == true ) {
                    string sqlStrTmp = (string)parametersObj.listSql.Value;
                    string optionToInsert = "<option></option>";
                    model.reportNameList = new SelectList(model.fillSelectForReports(_АppSettings.vestrancecommersString, sqlStrTmp), "key", "reportName");
                    foreach (var tmp in model.reportNameList){
                        optionToInsert += "<option value = \"" + tmp.Value.ToString() + "\">"+ tmp.Text.ToString() + "</option>";
                    }
                    tempParamObj[vLoopCount].parameterHtml = (string)tempParamObj[vLoopCount].parameterHtml.Value.ToString().Replace("{{{addOptions}}}", optionToInsert);

                }
                vLoopCount++;
            }
            return new JsonResult(resAsJSON);
        }

    }
}