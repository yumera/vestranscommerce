﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using startProject1.Models;
using static startProject1.Models.ContactsAddress;
using static startProject1.Models.OrderFormModel;
using static startProject1.Models.StationsViewModel;
using static startProject1.Models.StatusesController;
using static startProject1.Models.UsersViewModel;
using static startProject1.Models.PricesViewModel;

namespace startProject1.Controllers
{
    public class AdminController : Controller
    {
        private readonly AppSettings _АppSettings;

        public AdminController(IOptions<AppSettings> appSettings)
        {
            _АppSettings = appSettings.Value;
        }


        /*#####################################  START LOGIN FUNTIONS ########################################### */
        [Authorize]
        public IActionResult Users()
        {
            var model = new UsersViewModel();
            model.userRightSelectList = new SelectList(model.htmlSelectUserRights(_АppSettings.vestrancecommersString), "key", "selectLabel");
            var modelStaions = new StationsViewModel();
            model.userDefaultStationId = new SelectList(modelStaions.getStationListAsDropDown("", "1=1", _АppSettings.vestrancecommersString), "key", "selectLabel");
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
                ViewData["hereApiKey"] = _АppSettings.hereApikey;
            }
            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult selectStationsAutoComplete() {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            var modelStaions = new StationsViewModel();
            SelectList userDefaultStationId;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                userDefaultStationId = new SelectList(modelStaions.getStationListAsDropDown("", requestBody.whereClause, _АppSettings.vestrancecommersString), "key", "selectLabel");
            };

            return new JsonResult(userDefaultStationId);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveUser()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new UsersViewModel();
                Login newLogin = new Login()
                {
                    loginAutoId = 0,
                    loginName = requestBody.dataJSON.loginName,
                    loginPassword = requestBody.dataJSON.loginPassword,
                    userAddress = requestBody.dataJSON.userAddress,
                    userCity = requestBody.dataJSON.userCity,
                    userCountry = requestBody.dataJSON.userCountry,
                    userEmail = requestBody.dataJSON.userEmail,
                    userNames = requestBody.dataJSON.userNames,
                    userPhone = requestBody.dataJSON.userPhone,
                    userRightId = requestBody.dataJSON.userRightId,
                    userDefaultStationId = requestBody.dataJSON.userDefaultStationId
                };

                requestBody.resultNewObjectJSON = JsonConvert.SerializeObject(model.saveNewUser(newLogin, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult changePasswordUser()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.dataJSON.loginName;
                string loggedUserPassword = requestBody.dataJSON.loginPassword;
                var model = new UsersViewModel();

                requestBody.resultNewObjectJSON = JsonConvert.SerializeObject(model.changePasswordUser(loggedUser, loggedUserPassword, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult editUser()
        {
            dynamic requestBody;
            dynamic returnBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new UsersViewModel();
                Login editLogin = new Login()
                {
                    loginAutoId = requestBody.dataJSON.loginAutoId,
                    loginName = requestBody.dataJSON.loginName,
                    userAddress = requestBody.dataJSON.userAddress,
                    userCity = requestBody.dataJSON.userCity,
                    userCountry = requestBody.dataJSON.userCountry,
                    userEmail = requestBody.dataJSON.userEmail,
                    userNames = requestBody.dataJSON.userNames,
                    userPhone = requestBody.dataJSON.userPhone,
                    userRightId = requestBody.dataJSON.userRightId,
                    userDefaultStationId = requestBody.dataJSON.userDefaultStationId
                };

                //requestBody.resultNewObjectJSON = JsonConvert.SerializeObject(model.editUser(editLogin, _АppSettings.vestrancecommersString));
                returnBody = JsonConvert.SerializeObject(model.editUser(editLogin, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(returnBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult deleteUser()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                int loginAutoId = requestBody.dataJSON.loginAutoId;
                var model = new UsersViewModel();

                requestBody.resultNewObjectJSON = JsonConvert.SerializeObject(model.deleteUser(loginAutoId, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }


        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readUser()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
            };
            var model = new UsersViewModel();
            var result = model.getUserList(loggedUser, _АppSettings.vestrancecommersString);

            return Json(new
            {
                draw = 1,
                recordsTotal = 90,
                recordsFiltered = 90,
                data = result
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readUser_static()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new UsersViewModel();
                requestBody.objectListJSON = JsonConvert.SerializeObject(model.getUserList(loggedUser, _АppSettings.vestrancecommersString));
            };

            return new JsonResult(requestBody);
        }
        /*#####################################  END LOGIN FUNTIONS ########################################### */

        /*#####################################  START STATION FUNTIONS ########################################### */
        [Authorize]
        public IActionResult Stations()
        {
            var model = new StationsViewModel();
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
                ViewData["hereApiKey"] = _АppSettings.hereApikey;
            }

            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult saveStations()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new StationsViewModel();
                Stations newStation = new Stations
                {
                    stationAddress = requestBody.dataJSON.stationAddress,
                    stationCity = requestBody.dataJSON.stationCity,
                    stationAutoId = 0,
                    stationCountry = requestBody.dataJSON.stationCountry,
                    stationName = requestBody.dataJSON.stationName,
                    stationPhone = requestBody.dataJSON.stationPhone,
                    stationLongitude = requestBody.dataJSON.stationLongitude,
                    stationLatitude = requestBody.dataJSON.stationLatitude
                };

                requestBody.resultNewStationJSON = JsonConvert.SerializeObject(model.saveNewStation(newStation, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult deleteStations()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                var model = new StationsViewModel();
                string vStatonAutoID = (string)requestBody.dataJSON.stationAutoId;
                requestBody.resultNewStationJSON = JsonConvert.SerializeObject(model.deleteStation(vStatonAutoID, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult editStations()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new StationsViewModel();
                Stations newStation = new Stations
                {
                    stationAddress = requestBody.dataJSON.stationAddress,
                    stationCity = requestBody.dataJSON.stationCity,
                    stationAutoId = requestBody.dataJSON.stationAutoId,
                    stationCountry = requestBody.dataJSON.stationCountry,
                    stationName = requestBody.dataJSON.stationName,
                    stationPhone = requestBody.dataJSON.stationPhone,
                    stationLongitude = requestBody.dataJSON.stationLongitude,
                    stationLatitude = requestBody.dataJSON.stationLatitude
                };

                requestBody.resultNewStationJSON = JsonConvert.SerializeObject(model.editStation(newStation, _АppSettings.vestrancecommersString));
            };
            return new JsonResult(requestBody);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readStations()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
                //requestBody.data = JsonConvert.SerializeObject(model.getStationList(loggedUser, _АppSettings.vestrancecommersString));
            };
            var model = new StationsViewModel();
            var result = model.getStationList(loggedUser, _АppSettings.vestrancecommersString);

            return Json(new
            {
                draw = 1,
                recordsTotal = 90,
                recordsFiltered = 90,
                data = result
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult readStationsStatic()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                string loggedUser = requestBody.userName;
                var model = new StationsViewModel();
                requestBody.stationListJSON = JsonConvert.SerializeObject(model.getStationList(loggedUser, _АppSettings.vestrancecommersString));
            };

            return new JsonResult(requestBody);
        }

        /*#####################################  END STATION FUNTIONS ########################################### */

        /*#####################################  START CONTACT FUNTIONS ########################################### */
        [Authorize]
        public IActionResult ContactsAddress()
        {
            var model = new ContactsAddress();
            model.priceGroupList = new SelectList(model.htmlPriceGroupListClass(_АppSettings.vestrancecommersString), "key", "selectLabel");
            model.stationIdList = new SelectList(model.htmlStationIdListClass(_АppSettings.vestrancecommersString), "key", "selectLabel");
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
                ViewData["hereApiKey"] = _АppSettings.hereApikey;
            }
            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult callContactsData()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            string whereClause = "";
            string sortBy = "";
            int rowLength = 10;
            int vDraw = 1;
            int pageIndex = 0;
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
                //whereClause = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.whereClause).Value.ToString()) ? " and 1=1 " : requestBody.whereClause);
                //Get search construction
                if (Convert.ToInt32(requestBody.searchArrayLength.ToString()) > 0){
                    whereClause += " and ( ";
                    for (int i = 0; i < Convert.ToInt32(requestBody.searchArrayLength.ToString()); i++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArray[i].paramName;
                        paramToAdd.paramValue = requestBody.searchArray[i].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (i < Convert.ToInt32(requestBody.searchArrayLength.ToString()) - 1 ? " " + requestBody.searchArray[i].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                //Get global search construction
                if (Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) > 0){
                    whereClause += " and ( ";
                    for (int j = 0; j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()); j++)
                    {
                        buildWhereClause paramToAdd = new buildWhereClause();
                        paramToAdd.paramName = requestBody.searchArrayGlobal[j].paramName;
                        paramToAdd.paramValue = requestBody.searchArrayGlobal[j].paramValue;
                        searchArrayObject.Add(paramToAdd);
                        whereClause += paramToAdd.paramName + " like @" + paramToAdd.paramName + "" + (j < Convert.ToInt32(requestBody.searchArrayGlobalLength.ToString()) - 1 ? " " + requestBody.searchArrayGlobal[j].paramCondition + " " : "");
                    }
                    whereClause += ")";
                }
                sortBy = (String.IsNullOrEmpty(((Newtonsoft.Json.Linq.JValue)requestBody.sortBy).Value.ToString()) ? " order by autoId " : requestBody.sortBy);
                rowLength = int.Parse(((Newtonsoft.Json.Linq.JValue)requestBody.rowLength).Value.ToString());
                vDraw = requestBody.draw;
                pageIndex = requestBody.currentPage;
            };
            var model = new ContactsAddress();
            var result = model.getContactList(loggedUser, _АppSettings.vestrancecommersString, whereClause, searchArrayObject, sortBy, (pageIndex * rowLength).ToString(), rowLength.ToString());
            var resultDataTable = model.getDataTableCounts(_АppSettings.vestrancecommersString, whereClause, searchArrayObject);

            return Json(new
            {
                draw = vDraw,
                recordsTotal = resultDataTable.totalRecords,
                recordsFiltered = resultDataTable.filteredRecords,
                data = result
            });

        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult checkContactPhoneBeforeSave()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new ContactsAddress();
            Contact newContact = new Contact()
            {
                contactCompanyName = requestBody.dataJSON.contactCompanyName,
                contactFirstName = requestBody.dataJSON.contactFirstName,
                contactMidleName = requestBody.dataJSON.contactMidleName,
                contactLastName = requestBody.dataJSON.contactLastName,
                contactCountry = requestBody.dataJSON.contactCountry,
                contactCity = requestBody.dataJSON.contactCity,
                contactAddress = requestBody.dataJSON.contactAddress,
                contactPhone = requestBody.dataJSON.contactPhone,
                contactEmail = requestBody.dataJSON.contactEmail,
                contactOthers = "",
                contactLongitude = requestBody.dataJSON.contactLongitude,
                contactLatitude = requestBody.dataJSON.contactLatitude,
                priceGroupId = requestBody.dataJSON.priceGroupId
            };

            var userModel = new UsersViewModel();
            returnJSON = JsonConvert.SerializeObject(model.checkContactPhoneBoforeSave(newContact, _АppSettings.vestrancecommersString));
            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveContacts()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new ContactsAddress();
            Contact newContact = new Contact()
            {
                contactCompanyName = requestBody.dataJSON.contactCompanyName,
                contactFirstName = requestBody.dataJSON.contactFirstName,
                contactMidleName = requestBody.dataJSON.contactMidleName,
                contactLastName = requestBody.dataJSON.contactLastName,
                contactCountry = requestBody.dataJSON.contactCountry,
                contactCity = requestBody.dataJSON.contactCity,
                contactAddress = requestBody.dataJSON.contactAddress,
                contactPhone = requestBody.dataJSON.contactPhone,
                contactEmail = requestBody.dataJSON.contactEmail,
                contactOthers = "",
                contactLongitude = requestBody.dataJSON.contactLongitude,
                contactLatitude = requestBody.dataJSON.contactLatitude,
                priceGroupId = requestBody.dataJSON.priceGroupId
            };

            var userModel = new UsersViewModel();
            newContact.contactCreatedByUserId = userModel.getUserIdByName(System.Net.WebUtility.UrlDecode(loggedUser), "autoId", _АppSettings.vestrancecommersString);

            returnJSON = JsonConvert.SerializeObject(model.saveNewContact(newContact, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult editContacts()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new ContactsAddress();
            Contact editContacts = new Contact()
            {
                autoId = requestBody.dataJSON.contactAutoId,
                contactCompanyName = requestBody.dataJSON.contactCompanyName,
                contactFirstName = requestBody.dataJSON.contactFirstName,
                contactMidleName = requestBody.dataJSON.contactMidleName,
                contactLastName = requestBody.dataJSON.contactLastName,
                contactCountry = requestBody.dataJSON.contactCountry,
                contactCity = requestBody.dataJSON.contactCity,
                contactAddress = requestBody.dataJSON.contactAddress,
                contactPhone = requestBody.dataJSON.contactPhone,
                contactEmail = requestBody.dataJSON.contactEmail,
                contactOthers = "",
                contactLongitude = requestBody.dataJSON.contactLongitude,
                contactLatitude = requestBody.dataJSON.contactLatitude,
                priceGroupId = requestBody.dataJSON.priceGroupId
            };

            returnJSON = JsonConvert.SerializeObject(model.editContact(editContacts, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult deleteContacts()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new ContactsAddress();
            Contact editContacts = new Contact()
            {
                autoId = requestBody.dataJSON.contactAutoId
            };

            returnJSON = JsonConvert.SerializeObject(model.deleteContact(editContacts, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        /*#####################################  END CONTACT FUNTIONS ########################################### */

        /*#####################################  START STATUSES FUNCTIONS ########################################### */
        [Authorize]
        public IActionResult Statuses()
        {
            var model = new StatusesController();
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }
            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult readStatuses()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
            };

            var model = new StatusesController();
            var result = model.geStatuses(loggedUser, _АppSettings.vestrancecommersString);
            return Json(new
            {
                draw = 1,
                recordsTotal = result.totalCount,
                recordsFiltered = result.totalCount,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult createStatus()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var userModel = new UsersViewModel();
            var model = new StatusesController();
            statuses newStatus = new statuses() {
                statusCreateByUserId = userModel.getUserIdByName(System.Net.WebUtility.UrlDecode(loggedUser), "autoId", _АppSettings.vestrancecommersString),
                statusDescription = requestBody.dataJSON.statusDescription,
                statusType = requestBody.dataJSON.statusType
            };

            returnJSON = JsonConvert.SerializeObject(model.createStatus(newStatus, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult editStatus()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new StatusesController();
            statuses editStatus = new statuses() {
                statusType = requestBody.dataJSON.statusType,
                statusDescription = requestBody.dataJSON.statusDescription,
                autoId = requestBody.dataJSON.selectedAutoId,
                statusOrderFinished = requestBody.dataJSON.statusOrderFinished
            };

            returnJSON = JsonConvert.SerializeObject(model.editStatus(editStatus, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult deleteStatus()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new StatusesController();
            statuses deleteStatus = new statuses() {
                autoId = requestBody.dataJSON.selectedAutoId
            };

            returnJSON = JsonConvert.SerializeObject(model.deleteStatus(deleteStatus, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }
        /*#####################################  END STATUSES FUNCTIONS ########################################### */

        /*#####################################  START FILE IMPORT FUNCTIONS ########################################### */
        [Authorize]
        public IActionResult FileImport() {
            var model = new FileImportController();
            CookieOptions fileProcessCookie = new CookieOptions();
            fileProcessCookie.Expires = DateTime.Now.AddMilliseconds(10000000);

            Response.Cookies.Append("fileProcess", "0", fileProcessCookie);
            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }

            return View(model);
        }

        public static void StoreSessionValueInt(ISession session, string key, int value) {
            session.SetInt32(key, value);
        }

        private OrderFormModel.returnState SaveNewOrderByFile(string userName, string[] rowOfFile, string fileName)
        {
            OrderFormModel.returnState vReturn = new OrderFormModel.returnState();
            vReturn.resultState = true;
            vReturn.resultMessage = "";
            string loggedUser = userName;
            newOrderClass newOrderVar = new newOrderClass();
            var userModel = new UsersViewModel();
            var orderModel = new OrderFormModel();
            List<buildWhereClause> searchArrayObject = new List<buildWhereClause>();
            try
            {
                //Start collecting order information
                newOrderVar.orderCreatedByUserID = userModel.getUserIdByName(System.Net.WebUtility.UrlDecode(loggedUser), "autoId", _АppSettings.vestrancecommersString);
                newOrderVar.stationID = userModel.getUserIdByName(System.Net.WebUtility.UrlDecode(loggedUser), "userDefaultStationId", _АppSettings.vestrancecommersString);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "contactPhone", paramValue = rowOfFile[0] });
                newOrderVar.orderSenderId = (rowOfFile[0].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[contacts]", "autoId", "isNull(contactPhone, '') = @contactPhone", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "contactPhone", paramValue = rowOfFile[1] });
                newOrderVar.orderReceiverId = (rowOfFile[1].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[contacts]", "autoId", "isNull(contactPhone, '') = @contactPhone", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "ordertypeName", paramValue = rowOfFile[2] });
                newOrderVar.orderTypeId = (rowOfFile[2].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[orderTypes]", "autoId", "isNull(ordertypeName, '') = @ordertypeName", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                newOrderVar.orderIsDocument = Convert.ToBoolean((rowOfFile[3] == "0" ? "false" : "true"));
                newOrderVar.orderCount = Convert.ToInt32(rowOfFile[4]);
                newOrderVar.orderDescription = rowOfFile[5];
                newOrderVar.orderContent = rowOfFile[6];
                newOrderVar.orderWeight = float.Parse(rowOfFile[7]);
                newOrderVar.orderWidth = float.Parse(rowOfFile[8]);
                newOrderVar.orderHeight = float.Parse(rowOfFile[9]);
                newOrderVar.orderDepth = float.Parse(rowOfFile[10]);
                newOrderVar.chashOnDelivery = Convert.ToBoolean((rowOfFile[11] == "0" ? "false" : "true"));
                newOrderVar.chashOnDeliverySum = Convert.ToDecimal(rowOfFile[12]);
                newOrderVar.orderInsuranceSum = Convert.ToDecimal(rowOfFile[13]);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "chashOnDeliveryName", paramValue = rowOfFile[14] });
                newOrderVar.cashOnDeliveryTypeId = (rowOfFile[14].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[cashOnDeliveryTypes]", "autoId", "isNull(chashOnDeliveryName, '') = @chashOnDeliveryName", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                newOrderVar.orderDeliveryDate = DateTime.ParseExact(rowOfFile[15], "dd.MM.yyyy", null);
                newOrderVar.orderDeliveryTime = TimeSpan.Parse(rowOfFile[16]);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "description", paramValue = rowOfFile[17] });
                newOrderVar.orderCityPriceId = (rowOfFile[17].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[cityDeliveryPrices]", "autoId", "isNull(description, '') = @description", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                searchArrayObject.Clear();
                searchArrayObject.Add(new buildWhereClause() { paramName = "description", paramValue = rowOfFile[18] });
                newOrderVar.orderInterCityPriceId = (rowOfFile[18].Length > 0 ? Convert.ToInt32(userModel.getFieldByTableAndCondition("[dbo].[interCityPrices]", "autoId", "isNull(description, '') = @description", searchArrayObject, true, _АppSettings.vestrancecommersString)) : 0);
                newOrderVar.orderInterCityPriceIdExpress = Convert.ToBoolean((rowOfFile[19] == "0" ? "false" : "true"));
                searchArrayObject.Clear();
                //searchArrayObject.Add(new buildWhereClause() { paramName = "description", paramValue = rowOfFile[18] });
                //newOrderVar.orderExtraPricesId = (rowOfFile[20].Length > 0 ? userModel.getFieldByTableAndCondition("[dbo].[extraServicesPrices]", "stuff( (SELECT ', ' + cast(autoId as nvarchar(32))", "isNull(description, '') in ( '" + rowOfFile[20].Replace(",", "','").Replace(",' ", ",'") + "' ) FOR XML PATH('') ), 1, 2, '')", searchArrayObject, true, _АppSettings.vestrancecommersString) : "");
                //searchArrayObject.Add(new buildWhereClause() { paramName = "description", paramValue = "'"+rowOfFile[20].Replace(",", "','").Replace(",' ", ",'") + "'" });
                string[] descriptionArray = rowOfFile[20].Split(",");
                string parametersWhere = "";
                for ( var y = 0; y < descriptionArray.Length; y++){
                    searchArrayObject.Add(new buildWhereClause() { paramName = "description"+y.ToString(), paramValue = descriptionArray[y].ToString().Trim() });
                    parametersWhere += "@description" + y.ToString() +  (y < descriptionArray.Length - 1 ? ", " : "" );
                }
                newOrderVar.orderExtraPricesId = (rowOfFile[20].Length > 0 ? userModel.getFieldByTableAndCondition("[dbo].[extraServicesPrices]", "stuff( (SELECT ', ' + cast(autoId as nvarchar(32))", "isNull(description, '') in ( "+ parametersWhere + " ) FOR XML PATH('') ), 1, 2, '')", searchArrayObject, true, _АppSettings.vestrancecommersString) : "");
                newOrderVar.orderImportFileName = fileName;
                var calculatePrices = orderModel.calculatePrice(newOrderVar, loggedUser, _АppSettings.vestrancecommersString);
                if (calculatePrices.resultState)
                {
                    newOrderVar.orderPriceVat = calculatePrices.priceVat;
                    newOrderVar.orderPriceNoVat = calculatePrices.priceNoVat;
                }
                else {
                    vReturn.resultState = false;
                    vReturn.resultMessage = "Грешка при калкулиране на цена.";
                }

            }
            catch (Exception ex) {
                vReturn.resultState = false;
                vReturn.resultMessage += (vReturn.resultMessage.Length > 0 ? " | " : "") + ex.Message;
            }

            var vCheckOrderValues = checkOrderValues(newOrderVar);
            if (vReturn.resultState && vCheckOrderValues.resultState) { 
                vReturn = orderModel.saveOrder(newOrderVar, _АppSettings.vestrancecommersString);
            } else {
                vReturn.resultState = false;
                vReturn.resultMessage += (vReturn.resultMessage.Length > 0 ? " | " : "" ) + vCheckOrderValues.resultMessage;
            }

            return vReturn;
        }

        private OrderFormModel.returnState checkOrderValues(newOrderClass newOrderVar) {
            OrderFormModel.returnState vReturn = new OrderFormModel.returnState();
            vReturn.resultState = true;
            //Cannot have both city and intercity types
            vReturn.resultState = (vReturn.resultState && newOrderVar.orderCityPriceId > 0 && newOrderVar.orderInterCityPriceId > 0 ? false : vReturn.resultState);
            if (!vReturn.resultState) {
                vReturn.resultMessage = "Грешка при създаване на поръчка: Липсват данни за тип на плащане";
            }
            //Cannot have count == 0
            vReturn.resultState = (vReturn.resultState && newOrderVar.orderCount <= 0 ? false : vReturn.resultState);
            if (!vReturn.resultState) {
                vReturn.resultMessage = "Грешка при създаване на поръчка: Брой трябва да е по голямо от 0";
            }
            //Check order descriptio and content for later stage
            //vReturnState = (vReturnState && newOrderVar.orderDescription.Trim().Length == 0 && newOrderVar.orderContent.Trim().Length == 0 ? false : vReturnState);
            //Weight has to be > 0
            vReturn.resultState = (vReturn.resultState && newOrderVar.orderWeight <= 0 ? false : vReturn.resultState);
            if (!vReturn.resultState) {
                vReturn.resultMessage = "Грешка при създаване на поръчка: Тегло трябва да е по голямо от 0";
            }
            //Order type has to be set
            vReturn.resultState = (vReturn.resultState && newOrderVar.orderTypeId <= 0 ? false : vReturn.resultState);
            if (!vReturn.resultState) {
                vReturn.resultMessage = "Грешка при създаване на поръчка: Тип на поръчка е задълителен";
            }
            //Both sender and receiver has to be set
            vReturn.resultState = (vReturn.resultState && newOrderVar.orderSenderId <= 0 && newOrderVar.orderReceiverId <= 0 ? false : vReturn.resultState);
            if (!vReturn.resultState) {
                vReturn.resultMessage = "Грешка при създаване на поръчка: Поръчител и Подател задължителни";
            }

            return vReturn;
        }

        private class fileProcessClass{
            public bool fileProcessedStatus { get; set; }
            public string fileProcessedStatusBg { get; set; }
            public string returnMessage { get; set; }
            public int fileRecordCount { get; set; }
            public int successCount { get; set; }
            public int errorCount { get; set; }

            public fileProcessClass(){
                fileProcessedStatus = false;
                fileProcessedStatusBg = "";
                returnMessage = "";
                fileRecordCount = 0;
                successCount = 0;
                errorCount = 0;
            }
        }

        private string GetMimeType(string fileName){
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UloadFile(List<IFormFile> files )
        {
            long size = files.Sum(f => f.Length);
            fileProcessClass fileStatus = new fileProcessClass();
            string userName = "";
            if (!string.IsNullOrEmpty(Request.Form["userName"])){
                userName = Request.Form["userName"];
            }
            if (userName == "") {
                fileStatus.fileProcessedStatusBg = "Грешка";
                fileStatus.returnMessage = "<table><tr><td>Име на потребител не може да е празно.</td></tr>";
                return new JsonResult(fileStatus);
            }
            // full path to file in temp location
            var filePath = Path.GetTempFileName();

            foreach (var formFile in files){
                string fileType = GetMimeType(formFile.FileName);
                if (fileType != "application/vnd.ms-excel") {
                    fileStatus.fileProcessedStatusBg = "Грешка";
                    fileStatus.returnMessage = "<table><tr><td>Само файлове с разширение .csv са разрешени. fileType : " + fileType + "</td></tr>";
                    return new JsonResult(fileStatus);
                }
                if (formFile.Length > 0){
                    using (var stream = new FileStream(filePath, FileMode.Create)){
                        await formFile.CopyToAsync(stream);
                        //Example https://stackoverflow.com/questions/37769864/c-sharp-mvc-upload-progress-bar
                        //Example with progress bar : https://stackoverflow.com/questions/42164380/asp-net-core-file-upload-progress-session

                        StreamReader streamReader = new StreamReader(formFile.OpenReadStream());
                        string tmpStr;
                        int fileProcessStat = 0;
                        while (!streamReader.EndOfStream){
                            while ((tmpStr = streamReader.ReadLine()) != null){
                                string[] strToRead = tmpStr.Split(";".ToCharArray());
                                if (fileProcessStat > 0) {
                                    OrderFormModel.returnState saveFileReturnState = new OrderFormModel.returnState();
                                    saveFileReturnState = SaveNewOrderByFile(userName, strToRead, formFile.FileName);
                                    if (saveFileReturnState.resultState) {
                                        fileStatus.successCount++;
                                    } else {
                                        fileStatus.errorCount++;
                                        if ( fileStatus.errorCount == 1 ){
                                            fileStatus.returnMessage = "<table>";
                                        }
                                        fileStatus.returnMessage += "<tr><td>Номер линия в файла : " + fileProcessStat.ToString() + " </td><td> " + saveFileReturnState.resultMessage + "</td></tr>";
                                    }
                                }
                                //StoreSessionValueInt(HttpContext.Session, "fileProcess", fileProcessStat);
                                fileProcessStat++;
                            }
                        }

                        fileStatus.fileRecordCount = fileProcessStat;
                        fileStatus.fileProcessedStatus = (fileStatus.errorCount == 0);
                        fileStatus.fileProcessedStatusBg = (fileStatus.errorCount == 0 ? "Успех" : "Грешка");
                        fileStatus.returnMessage = (fileStatus.errorCount == 0 ? "Файла е импортиран успешно" : "Една или повече грешки са открити при импортването на файла : " + "<br/>" + fileStatus.returnMessage + "</table>");
                    }
                }
            }

            return new JsonResult(fileStatus);
        }

        /*#####################################  END FILE IMPORT FUNCTIONS ########################################### */

        /*#####################################  START PRICE FUNTIONS ########################################### */
        [Authorize]
        public IActionResult Prices()
        {
            var model = new PricesViewModel();
            model.weightLimitInterCityPrice = new SelectList(model.htmlFillWeights(), "key", "selectLabel");

            if (User.Identity.IsAuthenticated)
            {
                ViewData["ConnectionString"] = _АppSettings.vestrancecommersString;
            }

            return View(model);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult readCityPrices()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
            };

            var model = new CityPricesViewModel();
            var result = model.getCityPricesList(loggedUser, _АppSettings.vestrancecommersString);
            return Json(new
            {
                draw = 1,
                recordsTotal = result.totalCount,
                recordsFiltered = result.totalCount,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult createPrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new CityPricesViewModel();
            CityPricesViewModel.CityPrices newPrice = new CityPricesViewModel.CityPrices()
            {
                description = requestBody.dataJSON.descriptionCityPrice,
                deliveryTimeMinute = requestBody.dataJSON.deliveryTimeMinuteCityPrice,
                priceNoVat = requestBody.dataJSON.priceNoVatCityPrice,
                priceVat = requestBody.dataJSON.priceVatCityPrice,
                autoId = 0
            };

            returnJSON = JsonConvert.SerializeObject(model.saveCityPrice(newPrice, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult editPrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new CityPricesViewModel();
            CityPricesViewModel.CityPrices editPrice = new CityPricesViewModel.CityPrices()
            {
                description = requestBody.dataJSON.descriptionCityPrice,
                deliveryTimeMinute = requestBody.dataJSON.deliveryTimeMinuteCityPrice,
                priceNoVat = requestBody.dataJSON.priceNoVatCityPrice,
                priceVat = requestBody.dataJSON.priceVatCityPrice,
                autoId = requestBody.dataJSON.selectedAutoId
            };

            returnJSON = JsonConvert.SerializeObject(model.editCityPrice(editPrice, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult deletePrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };
            int selectedCityPriceAutoId = requestBody.dataJSON.selectedAutoId;
            var model = new CityPricesViewModel();

            returnJSON = JsonConvert.SerializeObject(model.deleteCityPrice(selectedCityPriceAutoId, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult readInterCityPrices()
        {
            dynamic requestBody;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.userName;
            };

            var model = new InterCityPricesViewModel();
            var result = model.getInterCityPricesList(loggedUser, _АppSettings.vestrancecommersString);
            return Json(new
            {
                draw = 1,
                recordsTotal = result.totalCount,
                recordsFiltered = result.totalCount,
                data = result.returnObject
            });
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult createInterCityPrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new InterCityPricesViewModel();
            InterCityPricesViewModel.InterCityPrices newInterPrice = new InterCityPricesViewModel.InterCityPrices()
            {
                description = requestBody.dataJSON.descriptionInterCityPrice,
                weightLimit = requestBody.dataJSON.weightLimitInterCityPrice,
                expressPriceNoVat = requestBody.dataJSON.expressPriceNoVatInterCityPrice,
                expressPriceVat = requestBody.dataJSON.expressPriceVatInterCityPrice,
                economicalPriceNoVat = requestBody.dataJSON.economicalPriceNoVatInterCityPrice,
                economicalPriceVat = requestBody.dataJSON.economicalPriceVatInterCityPrice,
                autoId = 0
            };

            returnJSON = JsonConvert.SerializeObject(model.saveInterCityPrice(newInterPrice, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult editInterCityPrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };

            var model = new InterCityPricesViewModel();
            InterCityPricesViewModel.InterCityPrices editInterCityPrice = new InterCityPricesViewModel.InterCityPrices()
            {
                description = requestBody.dataJSON.descriptionInterCityPrice,
                weightLimit = requestBody.dataJSON.weightLimitInterCityPrice,
                expressPriceNoVat = requestBody.dataJSON.expressPriceNoVatInterCityPrice,
                expressPriceVat = requestBody.dataJSON.expressPriceVatInterCityPrice,
                economicalPriceNoVat = requestBody.dataJSON.economicalPriceNoVatInterCityPrice,
                economicalPriceVat = requestBody.dataJSON.economicalPriceVatInterCityPrice,
                autoId = requestBody.dataJSON.selectedAutoId
            };

            returnJSON = JsonConvert.SerializeObject(model.editInterCityPrice(editInterCityPrice, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult deleteInterCityPrice()
        {
            dynamic requestBody;
            dynamic returnJSON;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            string loggedUser = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                requestBody = JObject.Parse(reader.ReadToEnd());
                loggedUser = requestBody.dataJSON.userName;
            };
            int selectedInterCityPriceAutoId = requestBody.dataJSON.selectedAutoId;
            var model = new InterCityPricesViewModel();

            returnJSON = JsonConvert.SerializeObject(model.deleteInterCityPrice(selectedInterCityPriceAutoId, _АppSettings.vestrancecommersString));

            return new JsonResult(returnJSON);
        }

        /*#####################################  END PRICE FUNTIONS ########################################### */

    }
}